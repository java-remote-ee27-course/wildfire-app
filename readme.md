# Wildfire App

See the Youtube video introducing [The Wildfire App](https://youtu.be/BTybVGzO__g?feature=shared)

The first bigger practical project at SDA JavaRemoteee27 (9.09-17.11.23) Size: 45.7 MB

**The App is still at the development phase right now.**

**State of the art:**
JavaFx graphical interface enables:

WILDFIRE TAB: 
- add Wildfire name, latitude, longitude, radius, coefficient values to the database "wildfire" (all except name have to be double, otherwise they will not be added, error messagee is shown)
- while saving wildfire info in DB, the "Map" tab shows all wildfires info right away.

MAP TAB: 
- All wildfires in Database are displayed in table. 
- Some demo wildfires are generated initially when the app starts
- When selecting one of the wildfires from wildfires table:
- - Open Street Map of the fire location is displayed from web (querying latitude, longitude from database). 
- - People in Red Zone are displayed in "People in Area" tab. 
- - Rescuers in area are displayed in "Rescuers in Area" tab (if there are any)
- After selecting a particular wildfire and pressing "Update wildfire" button, you can update the wildfire info (a new scene is displayed).
- After updating a wildfire, to show wildfires, press "Show wildfires" button.
- Wildfires can be deleted (from table+database) by pressing "Delete a wildfire" button. 
- If no wildfire is selected when trying to delete/update wildfire, the error modal is displayed.

UPDATE WILDFIRE WINDOW:
- update info of the selected wildfire. After pressing "Update" button, the wildfire info is updated in the database and the window is closed. 
- When trying to insert text in radius, latitude, longitude, coefficient field, the error is displayed and no data is sent to DB. Window remains open.

FIRESTATIONS TAB: 
- query firestations from the Wildfire DB Firestation table and see them in Firestations tab. (Right now all firestations are queried from DB)
- some demo firestations are generated initially when the app starts

PEOPLE IN AREA TAB: 
- query all residents from Wildfire database, from Person table and see them in People in Area tab. 
- see residents in "Red Zone" when selecting a wildfire from "Wildfire" tab. (Right now rough calculations are done with the line length formula TODO:: improve the formula)

- For demo purpose some Firestations, Persons, Wildfires and Rescuers are automatically generated when app starts 
  (see the code com.lunamezzogiorno.wildfireapp.data.DemoFirestationsGenerator, com.lunamezzogiorno.wildfireapp.data.DemoPersonsGenerator and com.lunamezzogiorno.wildfireapp.data.DemoWildfireGenerator

RESCUERS TAB:
- see the rescuers (after a wildfire is selected in the Map tab). 
- some demo rescuers are generated initially when app starts
- you can see rescuers in Red Zone. When no rescuers are connected to the wildfire yet, a notice is displayed
- Pressing "Add new rescuers in area" button opens a new view (new scene) where you can choose city, firestation from comboboxes, then the rescuers list of theselected firestation is displeyed. 
- Selecting and adding rescuers is still in develoipment, so the button "Add rescuers" does not work yet
TODO:: add possibility to update rescuers info (i.e. mark or unmark them working in Red Zone)

WEATHER TAB:
- when you choose a particular wildfire in Map tab, you can query the weather broadcast of 5 days / 3 hrs from the OpenWebMap. 5 days / 3 hrs API key is needed for that 

  **NB! "Call 5 day / 3 hour forecast data" API key is needed to fetch weather data when you clone the library. Get the API key from: https://openweathermap.org/forecast5**
  **API key: copy the file src/main/resources/app.properties.example into src/main/resources/app.properties and provide the API key.**

![images/wildfire_tab_23.09.23.png]() <img src="images/wildfire_tab_23.09.23.png" width="502">

![images/wildfire_tab_error_non_doubles_23.09.23.png]() <img src="images/wildfire_tab_error_non_doubles_23.09.23.png" width="502">

![images/map_tab_3.10.23.png]() <img src="images/map_tab_3.10.23.png" width="502">

![images/update_wildfire_info_initial_3.10.23.png]() <img src="images/update_wildfire_info_initial_3.10.23.png" width="502">

![images/update_wildfire_info_error_3.10.23.png]() <img src="images/update_wildfire_info_error_3.10.23.png" width="502">

![images/update_wildfire_info_changed_3.10.23.png]() <img src="images/update_wildfire_info_changed_3.10.23.png" width="502">

![images/map_tab_after_wildfire_show+del_7.10.23.png]() <img src="images/map_tab_after_wildfire_show%2Bdel_7.10.23.png"  width="502">

![images/map_tab_after_wildfire_show+del_7.10.23.png]() <img src="images/map_tab_after_wildfire_show%2Bdel_7.10.23.png" width="502">

![images/map_tab_error_nothing_selected_to_update_7.10.23.png]() <img src="images/map_tab_error_nothing_selected_to_update_7.10.23.png" width="502">

![images/map_tab_error_when_nothing_selected_to_del_7.10.23.png]() <img src="images/map_tab_error_when_nothing_selected_to_del_7.10.23.png" width="502">

![images/firestations_tab_19.09.23.png]() <img src="images/firestations_tab_19.09.23.png" width="502">

![images/people_in_area_tab_23.09.23.png]() <img src="images/people_in_area_tab_23.09.23.png" width="502">

![images/weather_tab_26.09.23.png]() <img src="images/weather_tab_26.09.23.png" width="502">

![rescuers_in_area_tab_08.11.23.png]() <img src="images/rescuers_in_area_tab_08.11.23.png" width="502">

![add_new_rescuers_in_area_view_08.11.23.png]() <img src="images%2Fadd_new_rescuers_in_area_view_08.11.23.png" width="502">

**The code:**
- The code is divided into controllers, data packages, weatherdata, demo generators. The data package contains beans, DAOs, interfaces, HibernateUtil
- Fxml files are for main scene and views.
- Login info etc are in app.properties file (the example of it is app.properties.example)

![images/wildfire_app_code_3.10.23.png](images/wildfire_app_code_3.10.23.png)

![images/wildfire_app_code_3.10.23_2.png](images/wildfire_app_code_3.10.23_2.png)

## Initial description

**The Wildfire app project idea:**

- Mark wildfire location at least in 3 ways:
  - Place + Lat + Long
  - Lat + long
  - Lat + long + radius
- Update wildfire location 
- Mark wildfire ended
- Get info of firestations nearby (from db)
- Get info of people nearby – in red zone / yellow zone. Let’s say that app uses a database of people living near by (query from db)
(ideally it could happen by getting ppl geolocations by gps-tracking or by mobile service provider location).
- Send an e-mail / message to people in red zone (query from db)
- Get weather data from web (wind, speed etc.) (web API)
- By the weather data count the fire moving direction (save to db?)
- Get info of people living zones where fire might proceed (from db)
- Get info of local resources (buses, helicopters, planes and other transportation) which could be sent to evacuate people (from db)
- App would allow to update info of people who have left or evacuated from the area (saves info to db)
- App would allow to update info of people who are temporarily at the red zone (firefighters, medics, tourists etc.)
- All data of wildfires, people, rescuers, firestations etc. is added to database (except weather data)
- App enables to fetch a map of wildfire (using Open Street Map) and fetch weather data from web

**Initial version:**
To keep it simpler, I assume:
- it to be an official app where the fire data could be added/updated by officials (so one fire will not be reported by hundreds of ppl)
- The mock database is supposed to be the state's official firestations database

**The initial database schema**
(To be updated)
- DB consists of different tables: wildfire, location, radius and person + junction tables firestations_locations, wildfire_locations etc. (see the picture below)
- location and radius are in many-to-one relationship (many locations can have the same radius)
- wildfire and location are one-to-one relationship (for simplicity and due to the possibility to mark wildfire radius - to be updated later so that the radius can be updated and the old data will be archived)
- firestation and location are in one-to-one relationship
- etc.

The radius table can have unique values only, if radius is already present, it will not be added as duplicate, instead the existing radius value is queried

![images/wildfireapp_database_schema_13_17.10.23.png](images/wildfireapp_database_schema_08.11.23.png)

## Prerequisites to run the code
- maven
- mysql
- intelliJ (optional)
- OpenWeatherMap API key (if you want to fetch weather data from web / however other functionalities work w/o it, too)

## Launch procedure

- git clone this repository locally 
- verify that mysql is running on the target system
- create a mysql database named: wildfire (or you may use the database dump included: Dump20230919_wildfireapp.sql)
- copy the file src/main/resources/app.properties.example into src/main/resources/app.properties and provide appropriate options
- Open Weather MAP "Call 5 day / 3 hour forecast data" API key to fetch weather data from OWM when you clone the library. Get the API key from: https://openweathermap.org/forecast5
- provide API key to your new src/main/resources/app.properties file
- the database tables will be created automatically because of the HBM2DDL_AUTO=create
- launch the application, the entry point is: com.lunamezzogiorno.wildfireapp.WildfireApp class

![images/img_2.png](images/img_2.png)

- build the Maven libraries if needed (see the [article](https://www.jetbrains.com/help/idea/build-sync-tool-window.html) )

## Feature list
Please see the "State of the art" chapter above.

## Technologies used
* JavaFx
* Open Street Map
* OpenWeatherMap
* Hibernate
* MySql
* Java version used in this project: 20

## How to contribute
Right now it is just a personal practical program

## TODO
- add functionality to show WF info right away when app loads
- update tables so that no duplicates can be added to the tables (wildfire, rescuers, persons and firestations tables)
- add calculations to discover where wildfire might spread
- find out why map is missing some "blocks" when fetching map from web
- design and implement transportation and message tabs
- DRY code (right now there are a few repetitions)
- add some Views from database (ViewRedZone, ViewPeopleInRedZone etc.)
- change people in area tab (right now updating people info does not work)
- add persons temporary address table
- finish the Add Rescuers view ("Rescuers in area" tab) - add possibility to update rescuers info (i.e. mark or unmark them working in Red Zone)
- show initial wildfire info after the update window is closed 
- radius allows no duplicates, but it blocks changing the wildfire radius to already existing radius (if some wildfire already has that radius)
- show overall map if no wildfire is selected and add markers for each of them
- improve the formula of calculating distances (right now the Earth curvature is not considered. Anyway, it has no impact with small distances and the calculations will remain rough as mountains and valleys are not considered as well)
- and many other things

## Author
Katlin Kalde





