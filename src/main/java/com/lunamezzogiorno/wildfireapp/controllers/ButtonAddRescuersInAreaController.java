package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.WildfireApp;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.util.Optional;

@AllArgsConstructor
@Getter
@Setter
public class ButtonAddRescuersInAreaController {
    private SessionFactory session;
    private Wildfire wildfire;
    @FXML private Button addRescuersInAreaButton;
    @FXML private TableView<Wildfire> wildfireTable;


    public ButtonAddRescuersInAreaController() {}

    public ButtonAddRescuersInAreaController(SessionFactory session) {
        this.session = session;
    }

    public void setButton(Button button){
        this.addRescuersInAreaButton = button;
        this.addRescuersInAreaButton.setOnMouseClicked(mouseEvent -> {

            var stage = (Stage) this.addRescuersInAreaButton.getScene().getWindow();
            var resource = WildfireApp.class.getResource("rescuersadd-view.fxml");
            var fxmlLoader = new FXMLLoader(resource);
            var addRescuerViewController = new AddRescuersViewController(session);
            fxmlLoader.setController(addRescuerViewController);
            getSelectedFireAndCreateScene(fxmlLoader, stage);
        });
    }

    //The optional isFireSelected() checks if a fire is selected.
    //If not, the NullpointerException is catched, the scene will not be created
    //and the Alert window is shown.
    public void getSelectedFireAndCreateScene(FXMLLoader fxmlLoader, Stage stage){
        try {
            if(isAFireSelected().isPresent())
                createAddRescuerScene(fxmlLoader, stage);

        } catch(NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.NONE);
            ButtonType type = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
            alert.getDialogPane().getButtonTypes().add(type);
            alert.setContentText("\nTo add rescuers info, please select a wildfire from Map view first!\n\n");
            alert.setTitle("Alert");
            alert.showAndWait();
            System.out.println(
                    e.getCause() + ", " +
                            e.getMessage() + " " +
                            e.getClass() + "\n-----\n" +
                            "No wildfire is selected in the Map tab to connect rescuer to it.\n" +
                            "Please select a wildfire first before pressing ADD!");
            e.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public Optional<Wildfire> isAFireSelected(){
        return Optional.of(wildfireTable.getSelectionModel().getSelectedItem());
    }

    public void createAddRescuerScene(FXMLLoader fxmlLoader, Stage stage) throws IOException {
        Parent root = fxmlLoader.load();
        stage.setTitle("Add rescuers");
        stage.setScene(new Scene(root, 650, 700));
    }
}


