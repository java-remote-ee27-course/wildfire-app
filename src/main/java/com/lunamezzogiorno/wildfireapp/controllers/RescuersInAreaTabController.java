package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.RedZoneCalculations;
import com.lunamezzogiorno.wildfireapp.data.dao.*;
import com.lunamezzogiorno.wildfireapp.data.entities.*;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.SessionFactory;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * RescuersInAreaTabController controls how rescuers are shown in Rescuers In Area Tab.
 * To show in App the rescuers in area, a wildfire must be selected first from the table in Map tab.
 * Rescuers in area are right now displayed by their "home" address which comes from the Person db table.
 * The wildfire (if any) connected to the rescuer (getWildfire) is used to compare if rescuer is in Red Zone
 * TODO:: right now it sometimes shows Corleone firefighter both for Corleone and Trapani.
 *  when you click first Corleone fire, then see rescuesrs in area tab,
 *  then go map tab, select Trapani fire and see rescuers in area tab.
 *  It gives an error when trying to select Trapani WF and see rescuers in area. Connection leak.
 *  Exception in thread "JavaFX Application Thread" java.lang.NullPointerException:
 *  Cannot invoke "Object.equals(Object)" because the return value of "com.lunamezzogiorno.wildfireapp.data.entities.Rescuer.getWildfire()" is null
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RescuersInAreaTabController {
    @FXML private Button addRescuersInAreaButton;
    @FXML private TableView<Rescuer> rescuersInAreaTable;
    @FXML private TableColumn<Rescuer, String> rescuerIdCol;
    @FXML private TableColumn<Rescuer, String> rescuerNameCol;
    @FXML private TableColumn<Rescuer, String> rescuerLastnameCol;
    @FXML private TableColumn<Rescuer, String> rescuerStationCol;
    @FXML private TableColumn<Rescuer, String> rescuerStreet1Col;
    @FXML private TableColumn<Rescuer, String> rescuerStreet2Col;
    @FXML private TableColumn<Rescuer, String> rescuerHomeCityCol;
    @FXML private Label wildfireNameInRescuersLabel;
    @FXML private Label noRescuersAddedLabel;
    private SessionFactory session;
    private ObservableList<Rescuer> rescuers = FXCollections.observableArrayList();
    private RedZoneCalculations redZoneCalculations;
    private Wildfire wildfire;
    private Firestation firestation;
    private RescuerDao rescuerDao;
    private FirestationDao firestationDao;
    private WildfireDao wildfires;
    private PersonDao personDao;



    public RescuersInAreaTabController(SessionFactory session) {
        this.session = session;
    }

    public void setButton(Button button) {
        this.addRescuersInAreaButton = button;
        addRescuersInAreaButton.setOnAction(ActionEvent -> {
            //emptyRescuersTableFields();
            var ses = session.openSession();
            var tx = ses.beginTransaction();
            rescuerDao = new RescuerDao(ses);
            firestationDao = new FirestationDao(ses);
            personDao = new PersonDao(ses);
            wildfire = new Wildfire();

            //showAllRescuersInApp();
            tx.commit();
            ses.close();
        });
    }

    public void displayWildfireName(Wildfire wildfire){
        wildfireNameInRescuersLabel.setText(wildfire.getName());
    }

    public void showAllRescuersInApp() {
        emptyRescuersTableFields();
        emptyNoRescuersAddedWarning();

        try {
            rescuers.addAll(getRescuersInThisWildfireRedZone());
            rescuerIdCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getId()
                    .toString()
            ));
            rescuerNameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getPerson()
                    .getName()
            ));
            rescuerLastnameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getPerson()
                    .getLastname()
            ));
            rescuerStationCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getFirestation()
                    .getName()));
            rescuerHomeCityCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getPerson()
                    .getAddress()
                    .getCity()));
            rescuersInAreaTable.setItems(rescuers);
        } catch (Exception ex){
            ex.printStackTrace();
            /**
            emptyRescuersTableFields();
            System.out.println("No firefighters were found connected to the wildfire / in red zone.");
            System.out.println(ex + ", " +
                    ex.getMessage() + ", " +
                    ex.getStackTrace()[0].getMethodName() +
                    " in rows: " + ex.getStackTrace()[0].getLineNumber() +
                    ", " + ex.getStackTrace()[1].getLineNumber() +
                    ", " + ex.getStackTrace()[2].getLineNumber() +
                    ", " + ex.getStackTrace()[3].getLineNumber() +
                    " in class: " +
                    this.getClass() +
                    " or in the class that it calls");
             */
        }
    }

    public void emptyRescuersTableFields() {
        rescuers.clear();
    }

    public void emptyNoRescuersAddedWarning(){
        noRescuersAddedLabel.setText("");
    }

    private List<Rescuer> getRescuersInThisWildfireRedZone(){
            List<Rescuer> rescuers = new ArrayList<>();
            getAllRescuers()
                    .stream()
                    .filter(r -> r.getWildfire() != null && r.getWildfire().equals(wildfire))
                    .forEach(rescuers::add);
            if(rescuers.isEmpty()){
                noRescuersAddedLabel.setText("No rescuers connected to the wildfire yet!");
                System.out.println("No rescuers added yet!");
            }
            return rescuers;
    }

    //Get rescuers in red zone (who are connected to the selected wildfire)
    private List <Rescuer> getAllRescuers(){
        return getAllFirestations()
                .stream()
                .map(Firestation::getRescuers)
                .flatMap(Collection::stream)
                .toList();
    }

    public List<Firestation> getAllFirestations(){

        return getAllObjects(firestationDao);
    }

    public <T, V> List<T> getAllObjects(SelectableDao<T, V> s){
        return s.getAll();
    }

}
