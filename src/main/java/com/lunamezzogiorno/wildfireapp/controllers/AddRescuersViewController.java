package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.DataSingleton;
import com.lunamezzogiorno.wildfireapp.WildfireApp;
import com.lunamezzogiorno.wildfireapp.data.dao.AddressDao;
import com.lunamezzogiorno.wildfireapp.data.dao.FirestationDao;
import com.lunamezzogiorno.wildfireapp.data.dao.RescuerDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import com.lunamezzogiorno.wildfireapp.data.entities.Firestation;
import com.lunamezzogiorno.wildfireapp.data.entities.Rescuer;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public class AddRescuersViewController implements Initializable {
    @FXML private Label addRescuersViewWildfireLabel;
    @FXML private ComboBox<String> addRescuerCityNameCombo;
    @FXML private ComboBox<String> addRescuerFirestationNameCombo;
    @FXML private Button addRescuerViewButton;
    @FXML private TableView<Rescuer> addRescuersTableView;
    @FXML private TableColumn<Rescuer, String> addRescuerIdCol;
    @FXML private TableColumn<Rescuer, String> addRescuerNameCol;
    @FXML private TableColumn<Rescuer, String> addRescuerLastnameCol;
    @FXML private TableColumn<Rescuer, String> addRescuerStationCol;
    @FXML private TableColumn<Rescuer, String> addRescuerPhoneCol;

    private SessionFactory session;
    private Wildfire wildfire;
    private List<Rescuer> listOfRescuers;
    private List<Address> listOfAddresses;
    private List<Firestation> listOfFirestations;
    private RescuerDao rescuerDao;
    private AddressDao addressDao;
    private FirestationDao firestationDao;

    private ObservableList<Rescuer> rescuers = FXCollections.observableArrayList();
    private ObservableList<String> addresses = FXCollections.observableArrayList();
    private ObservableList<String> firestations = FXCollections.observableArrayList();

    private final String[] city = {null};
    private Firestation firestationByName;
    private ChangeListener<String> cityChangeListener;
    private ButtonAddRescuersInAreaController buttonAddRescuersInAreaController;

    DataSingleton data = DataSingleton.getInstance();

    public AddRescuersViewController() {}

    public AddRescuersViewController(SessionFactory session) {
        this.session = session;
    }

    //set to new stage the initial data:
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        //Button which opens rescuers update form, do I need it ?:
        //buttonAddRescuersInAreaController = new ButtonAddRescuersInAreaController(session);

        var ses = session.openSession();
        var tx = ses.beginTransaction();
        wildfire = data.getWildfire();
        addRescuersViewWildfireLabel.setText("Rescuers in area of "+ wildfire.getName());

        //get all addresses:
        addressDao = new AddressDao(ses);
        listOfAddresses = addressDao.getAll();

        //initialize combobox w. cities list:
        setAddRescuerCityNameCombo(addRescuerCityNameCombo);
        showCityNamesToSelect();

        //set the Cities combo box to listen selections user made:
        setCitySelectionListener();

        /**************** LAST THING TO DO: Button to confirm selection and return to main view: *****************/
        setButton();

        tx.commit();
        ses.close();

    }

    //initially show city names in city selecton combo-box
    public void showCityNamesToSelect(){
        //add all city names to combo, converting to set to remove repetitions
        addRescuerCityNameCombo.getItems().addAll((listOfAddresses
                .stream()
                .map(Address::getCity)
                .collect(Collectors.toSet())));
    }

    // The next part listens to if city was selected
    // and populates the firetations selection combo accordingly with FS-s names
    public void setCitySelectionListener(){
        cityChangeListener = (obs, oldVal, newVal) -> {
            emptyAddRescuersTableFields();
            city[0] = newVal;

            var ses = session.openSession();
            var tx = ses.beginTransaction();

            //initialize firestations selection:
            firestationDao = new FirestationDao(ses);
            listOfFirestations = firestationDao.getAll();

            setAddRescuerFirestationNameCombo(addRescuerFirestationNameCombo);
            showFirestationNamesToSelect(city[0]);

            tx.commit();
            ses.close();

        };
        addRescuerCityNameCombo.valueProperty().addListener(cityChangeListener);
        //set the Firestations combo box to listen selections user made
        setFirestationSelectionListener();
    }


    public void showFirestationNamesToSelect(String city){
        addRescuerFirestationNameCombo.getItems().clear();
        System.out.println("FETCH FS: " + getFirestationNames(city));
        addRescuerFirestationNameCombo.getItems().addAll(
                (getFirestationNames(city)
                        .stream()
                        .toList()));
    }

    public List<String> getFirestationNames(String city){
        return getFirestationsByMatchingCity(city)
                .stream()
                .map(Firestation::getName)
                .toList();
    }

    public List<Firestation> getFirestationsByMatchingCity(String city){
        List<Firestation> firestationsInTheCity = new ArrayList<>();
        for(Firestation f : listOfFirestations){
            if(f.getAddress() != null && f.getAddress().getCity().equals(city)){
                firestationsInTheCity.add(f);
            }
        }
        return firestationsInTheCity;
    }

    // The next part listens to if firestation was selected
    // and populates the rescuers table accordingly with rescuers info
    public void setFirestationSelectionListener(){
        final String[] firestation ={null};
        ChangeListener <String> firestationChangeListener = (obs, oldVal, newVal) -> {
            firestation[0] = newVal;
            if(newVal != null) {
                //remove city selection combo listener that there would be no duplicate queries to DB
                addRescuerCityNameCombo.valueProperty().removeListener(cityChangeListener);

                System.out.println("The name of the FS is " + newVal + " is : " + firestation[0]);
                var ses = session.openSession();
                var tx = ses.beginTransaction();
                firestationDao = new FirestationDao(ses);
                firestationByName = firestationDao.getFirestationByName(firestation[0]);

                //Print and show rescuers info in table:
                System.out.println("RESCUERS: " + getRescuersOfSelectedFirestation());
                showAllRescuersToAddInApp();
                tx.commit();
                ses.close();

                //add city selection listener to listen city changes again
                addRescuerCityNameCombo.valueProperty().addListener(cityChangeListener);
            }
        };
        addRescuerFirestationNameCombo.valueProperty().addListener(firestationChangeListener);
    }

    //TODO:: this method is actually similar to RescuersInAreaTabController.showAllRescuersInApp() method, try to generify
    //Populate the tableview with selected firestations rescuers info
    public void showAllRescuersToAddInApp(){
        emptyAddRescuersTableFields();
        try {
            rescuers.addAll(getRescuersOfSelectedFirestation());
            addRescuerIdCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getId()
                    .toString()
            ));
            addRescuerNameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getPerson()
                    .getName()
            ));
            addRescuerLastnameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getPerson()
                    .getLastname()
            ));
            addRescuerStationCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                    .getValue()
                    .getFirestation()
                    .getName()));
            addRescuersTableView.setItems(rescuers);
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void emptyAddRescuersTableFields() {
        rescuers.clear();
    }

    //Get rescuers only from a selected firestation
    public List<Rescuer> getRescuersOfSelectedFirestation(){
        Set <Rescuer> rescuerSet = firestationByName.getRescuers();
        return rescuerSet.stream().toList();
    }


    //TODO:: the button click only brings back to main window, no changes are saved or made.
    // Add logic to save changes and add rescuers, see for multiple rows selection:
    // https://stackoverflow.com/questions/39365578/multi-select-in-tableview-javafx
    //Go back to main window:
    public void setButton(){
        addRescuerViewButton.setOnAction(actionEvent -> {
            try {
                goBackToMainWindow(actionEvent);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
    public void goBackToMainWindow(ActionEvent event) throws IOException {
        Stage stage = (Stage) this.addRescuerViewButton.getScene().getWindow();
        var resource = WildfireApp.class.getResource("wildfireapp-view.fxml");
        var fxmlLoader = new FXMLLoader(resource);
        var appController = new AppController(session);
        fxmlLoader.setController(appController);
        Parent root = fxmlLoader.load();
        stage.setTitle("Wildfire app");
        stage.setScene(new Scene(root, 650, 800));
    }
}
