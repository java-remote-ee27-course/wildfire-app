package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.dao.*;
import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Radius;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import lombok.*;
import org.hibernate.SessionFactory;
import java.util.*;

//TODO::find why the exception is shown in console when trying to add loc + lat non numbers - should be displayed on app
@AllArgsConstructor
@Getter
@Setter
public class WildfireTabController {
    @FXML public Button addLocationButton;
    @FXML private TextField fireNameText;
    @FXML private TextField latitudeText;
    @FXML private TextField longitudeText;
    @FXML private TextField radiusText;
    @FXML private TextField redZoneCoefficientText;
    @FXML private Label errorLabel;
    @FXML private TableView<Wildfire> wildfireTable;
    @FXML private TableColumn<Wildfire, String> wildfireIdCol;
    @FXML private TableColumn<Wildfire, String> wildfireNameCol;
    @FXML private TableColumn<Wildfire, String> wildfireLatitudeCol;
    @FXML private TableColumn<Wildfire, String> wildfireLongitudeCol;
    @FXML private TableColumn<Wildfire, String> wildfireRadiusCol;
    @FXML private TableColumn<Wildfire, String> redZoneTextCol;

    private SessionFactory session;
    private Location location;
    private LocationDao locations;

    private Radius radius;
    private RadiusDao radii2;

    private Wildfire wildfire;
    private WildfireDao wildfires;
    private MapTabController mapsView;


    public WildfireTabController(){}
    public WildfireTabController(SessionFactory session) {
        this.session = session;
    }

    public void setButton(Button button){
        mapsView = new MapTabController(session);
        this.addLocationButton = button;
        addLocationButton.setOnAction(actionEvent -> {
            var ses = session.openSession();
            var tx = ses.beginTransaction();

            locations = new LocationDao(ses);
            wildfires = new WildfireDao(ses);
            radii2 = new RadiusDao(ses);

            saveWildfireInfo();

            // pressing Wildfire tab "Add wildfire" button
            // also displays all wildfires info in Maps tab:

            displayWildfireInfoInMapTab();

            tx.commit();
            ses.close();

            emptyInputTextFields();
        });
    }


    public void saveWildfireInfo(){
        //wipe away previous error value and save data from form.
        errorLabel.setText("");
        try {
            getWildfireData(radii2);
        }
        catch (NumberFormatException ex){
            errorLabel.setText("Radius, location and coefficient must be numbers!");
            return;
        }
        radii2.save(radius);
        locations.save(location);
        wildfires.save(wildfire);

    }

    public void emptyInputTextFields(){
        latitudeText.setText("");
        longitudeText.setText("");
        radiusText.setText("");
        fireNameText.setText("");
        redZoneCoefficientText.setText("");
    }

    public void checkIfDoubleTextFieldIsBlank(){
        //check if field value is blank, if so, convert it to "0.0"
        List<TextField> textFields = new ArrayList<>(List.of(
                fireNameText,
                latitudeText,
                longitudeText,
                radiusText,
                redZoneCoefficientText));

        for(TextField t : textFields) {
            if (!(t == fireNameText) && t.getText().isBlank()) t.setText("0.0");
        }
    }

    public void getWildfireData(RadiusDao radiusDao){
        checkIfDoubleTextFieldIsBlank();
        //Check if radius value is present in db -- before creating the object.
        //If you check it after object is created, it becomes transient w/o saving to DB

        Double [] radiusObjectRadius = {getRadiusDouble()};

        if(isObjectPresent(radiusDao, radiusObjectRadius))
            radius = getObjectFromDatabase(radiusDao, radiusObjectRadius).orElse(new Radius());//orElseThrow();
        else radius = new Radius();

        //create new instances and set their fields
        // (set relations, e.g. radius to location etc.)
        location = new Location();
        wildfire = new Wildfire();
        wildfire.setLocation(location);
        wildfire.setRadius(radius);

        location.setLatitude(getLatitudeDouble());
        location.setLongitude(getLongitudeDouble());
        wildfire.setRedZoneCoefficient(getRedZoneCoefficientDouble());
        radius.setRadius(getRadiusDouble());
        wildfire.setName(fireNameText.getText());
    }

    // if Radius obj's radius field value is present in DB, get existing value,
    // do not create a new row with duplicate value to DB:
    private <T> Boolean isObjectPresent(EvaluativeDao<T> t, Double[] value){
        return t.isPresent(value);
    }

    //retrieving an object from DB by its field value(s) (e.g, by radius or by lat+lon)
    private <T, V> Optional<T> getObjectFromDatabase(SelectableDao<T, V> t, V [] value){
        return t.get(value);
    }

    //convert text field value to Double
    private Double getRadiusDouble(){
        return Double.parseDouble(radiusText.getText());
    }
    private Double getLatitudeDouble(){
        return Double.parseDouble(latitudeText.getText());
    }
    private Double getLongitudeDouble(){
        return Double.parseDouble(longitudeText.getText());
    }
    private Double getRedZoneCoefficientDouble(){
        return Double.parseDouble(redZoneCoefficientText.getText());
    }

    //The changes that happen on map tab pressing "Add wildfire" button in Wildfire tab:
    public void displayWildfireInfoInMapTab(){
        setMapTabControllerParameters();
        emptyMapTabFields();
        showAllWildfiresOnMapTabTable();
    }


    public void setMapTabControllerParameters(){
        mapsView.setWildfires(wildfires);
        mapsView.setWildfireIdCol(wildfireIdCol);
        mapsView.setWildfireNameCol(wildfireNameCol);
        mapsView.setWildfireTable(wildfireTable);
        mapsView.setWildfireLatitudeCol(wildfireLatitudeCol);
        mapsView.setWildfireLongitudeCol(wildfireLongitudeCol);
        mapsView.setWildfireRadiusCol(wildfireRadiusCol);
        mapsView.setRedZoneTextCol(redZoneTextCol);
    }
    public void emptyMapTabFields(){
        mapsView.emptyWildfireTableFields();
    }
    public void showAllWildfiresOnMapTabTable(){
        mapsView.showAllWildfiresInApp();
    }

}
