package com.lunamezzogiorno.wildfireapp.controllers;

import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import lombok.Getter;
import lombok.Setter;

/**
 * This is WebViewController class for displaying maps in "Map" tab
 * For the further investigation and development:
 * Google maps API:
 * Probably the best and most convenient to use and has lots of docs in web.
 * Free to use for the requests in amount 200$ per one month
 * Needs account backed with credit card, there should be an option to limit the requests per months
 * (in order to block probable attacks by some malicious bots which might increase the traffic)
 * https://dlsc-software-consulting-gmbh.github.io/GMapsFX/
 *
 * OSM API, examples of JavaFx OSM (the minus using it is that in JavaFx it loads poorly, some tiles missing):
 * https://github.com/gluonhq/maps -- probably the best to use with JavaFx
 * https://github.com/westnordost/osmapi -- the next however documentation is a bit hard to find to use w. Javafx
 * https://github.com/makbn/java_leaflet -- JavaFx, however assumes probably to have the project to be downloaded
 * https://www.sothawo.com/projects/mapjfx/ -- out of maintenance since 2022, however can be used
 * https://wiki.openstreetmap.org/wiki/Java_Access_Example
 * http://java-buddy.blogspot.com/2012/05/
 * https://neis-one.org/2017/10/processing-osm-data-java/
 *
 * Examples of Javascript of OSM
 * https://stackoverflow.com/questions/70923595/openlayers-add-marker-by-doubleclick-on-the-map
 * https://wiki.openstreetmap.org/wiki/OpenLayers_Marker_Example
 * https://leafletjs.com/ -- This could be used Java + Javascript
 *
 * Building a web browser in JavaFx:
 * https://www.youtube.com/watch?v=96r3olimdkA&ab_channel=BroCode
 * https://docs.oracle.com/javafx/2/webview/jfxpub-webview.htm
 */

@Setter
@Getter
public class WebViewController {
    @FXML private WebView mapWebView;
    private WebEngine webEngine;

    public void loadPage(Double latitude, Double longitude) {
        webEngine = mapWebView.getEngine();
        webEngine.load("https://www.openstreetmap.org/?mlat=" + latitude + "&mlon=" + longitude + "#map=" + "15/" + latitude + "/" + longitude);
        System.out.println("LATITUDE: " + latitude + " LONG: " + longitude);

    }
}
