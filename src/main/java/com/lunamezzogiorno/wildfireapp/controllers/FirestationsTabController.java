package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.dao.FirestationDao;
import com.lunamezzogiorno.wildfireapp.data.dao.LocationDao;
import com.lunamezzogiorno.wildfireapp.data.dao.SelectableDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Firestation;
import com.lunamezzogiorno.wildfireapp.data.entities.Phone;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.*;
import org.hibernate.SessionFactory;
import java.util.*;

/**
 * FirestationsTabController controls how firestations are shown in the Firestations tab.
 * When pressing the button "Show firestations",
 * all firestations are displayed with no selection which is nearest to the wildfire.
 * TODO::implement firestations search, which searches the nearest firestations to particular wildfire
 * TODO::implement rules how the nearest firestations are shown
 * Got the ideas from:
 * https://medium.com/@keeptoo/adding-data-to-javafx-tableview-stepwise-df582acbae4f
 * https://stackoverflow.com/questions/68243155/how-to-show-data-in-tableview-of-nested-objects
 * https://stackoverflow.com/questions/68243155/how-to-show-data-in-tableview-of-nested-objects
 * https://www.baeldung.com/hibernate-initialize-proxy-exception (fetch type eager for location)
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FirestationsTabController {
    @FXML private TableView<Firestation> firestationTable;
    @FXML public TableColumn<Firestation, String> firestationNameCol;
    @FXML public TableColumn<Firestation, String> firestationLatitudeCol;
    @FXML public TableColumn<Firestation, String>firestationLongitudeCol;
    @FXML public TableColumn<Firestation, String> firestationPhoneCol;
    @FXML public Button searchStationsButton;

    private SessionFactory session;
    private LocationDao locations;
    private FirestationDao firestations;

    public FirestationsTabController(SessionFactory session) {
        this.session = session;
    }

    public void setButton(Button button){
        this.searchStationsButton = button;

        searchStationsButton.setOnAction(actionEvent -> {
            emptyFirestationTableFields();
            var ses = session.openSession();
            var tx = ses.beginTransaction();
            firestations = new FirestationDao(ses);
            showAllFirestationsInApp();

            tx.commit();
            ses.close();

        });
    }

    //Empty fields when doing a new query (pushing Search btn)
    public void emptyFirestationTableFields(){
        stations.clear();
    }

    public <T, V> List<T> getAllStations(SelectableDao<T, V> s){
        return s.getAll();
    }

    private ObservableList<Firestation> stations = FXCollections.observableArrayList();

    //add all rows from DB to the Javafx firestations gridtable on "Firestations" tab:
    public void showAllFirestationsInApp(){

        stations.addAll(getAllStations(firestations));
        firestationNameCol.setCellValueFactory(new PropertyValueFactory<>("Name"));
        firestationLatitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getLocation()
                .getLatitude()
                .toString()));
        firestationLongitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getLocation()
                .getLongitude()
                .toString()));
        //TODO:: see later if .orElseThrow() is good idea here:
        firestationPhoneCol.setCellValueFactory(data -> new SimpleStringProperty(data
                .getValue()
                .getPhones()
                .stream()
                .map(Phone::getNumber)
                .findAny()
                .orElse(0)
                .toString()));
        firestationTable.setItems(stations);
    }
}
