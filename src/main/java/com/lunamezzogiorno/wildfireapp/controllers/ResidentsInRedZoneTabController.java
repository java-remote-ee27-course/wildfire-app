package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.RedZoneCalculations;
import com.lunamezzogiorno.wildfireapp.data.dao.PersonDao;
import com.lunamezzogiorno.wildfireapp.data.dao.SelectableDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResidentsInRedZoneTabController {
    @FXML
    private TableView<Person> peopleRedZoneTable;
    @FXML private TableColumn<Person, String> personRedZoneIdCol;
    @FXML private TableColumn<Person, String> personRedZoneLastnameCol;
    @FXML private TableColumn<Person, String> personRedZoneDistrictCol;
    @FXML private TableColumn<Person, String> personRedZoneCityCol;
    @FXML private TableColumn<Person, String> personRedZoneStreetCol;
    @FXML private TableColumn<Person, String> personRedZonePhoneCol;
    @FXML private TableColumn<Person, String> personRedZoneLatitudeCol;
    @FXML private TableColumn<Person, String> personRedZoneLongitudeCol;

    private PersonDao people;
    private RedZoneCalculations redZoneCalculations;
    private Wildfire wildfire;
    private ObservableList<Person> personsInRedZone = FXCollections.observableArrayList();



    public void showResidentsInRedZoneInApp(){
        emptyPersonsInRedZoneTableFields();
        personsInRedZone.addAll(getResidentsInRedZone());
        personRedZoneIdCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getId()
                .toString()));
        personRedZoneLastnameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getLastname()));
        personRedZoneDistrictCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getDistrict()));
        personRedZoneCityCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getCity()));
        personRedZoneStreetCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getStreet()));
        personRedZoneLatitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getLocation()
                .getLatitude()
                .toString()));
        personRedZoneLongitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getLocation()
                .getLongitude()
                .toString()));
        peopleRedZoneTable.setItems(personsInRedZone);
    }

    public void emptyPersonsInRedZoneTableFields(){
        //each time before loading new data to table, empty old (in order not to show the same data twice or more)
        personsInRedZone.clear();
    }

    public <T, V> List<T> getAllPeople(SelectableDao<T, V> s){
        return s.getAll();
    }

    //add all people in Red Zone from DB to the Javafx
    // "Show people in affected areas (Red Zone)" gridtable on "People in Area" tab:

    public List<Person> getResidentsInRedZone(){
        redZoneCalculations = new RedZoneCalculations(wildfire, getAllPeople(people));
        return redZoneCalculations.getDataOfResidentsInRedZone();
    }
}
