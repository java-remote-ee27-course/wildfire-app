package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.entities.Firestation;
import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import com.lunamezzogiorno.wildfireapp.data.entities.Rescuer;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import com.lunamezzogiorno.wildfireapp.weatherdata.WeatherList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.hibernate.SessionFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * NB! MapTabController class is not needed to be called in AppController class,
 * as WildfireTabController class calls MapTabController class
 * then MapController is injected in WildfireTabController class and it is instantiated there,
 * so it becomes part of WildfireTabController class.
 * WildfireTabController class is injected in AppController class here, called and instantiated here.
 *
 * TODO::For adding graphics later: https://www.youtube.com/watch?v=KzfhgGGzWMQ&ab_channel=KensoftPH
 * TODO::to implement WebView and maps: https://docs.oracle.com/javafx/2/webview/jfxpub-webview.htm,
 ** https://stackoverflow.com/questions/29324811/javafx-open-webview-in-fxml-document
 ** https://developers.google.com/maps/apis-by-platform
 ** https://developers.google.com/maps/documentation/embed/get-started
 */
public class AppController {
    private SessionFactory session;
    WildfireTabController wildfireTabController;
    @FXML private Button addLocationButton;
    @FXML private TextField fireNameText;
    @FXML private TextField latitudeText;
    @FXML private TextField longitudeText;
    @FXML private TextField redZoneCoefficientText;
    @FXML private TextField radiusText;
    @FXML private Label errorLabel;

    @FXML private TableView<Wildfire> wildfireTable;
    @FXML private TableColumn<Wildfire, String> wildfireIdCol;
    @FXML private TableColumn<Wildfire, String> wildfireNameCol;
    @FXML private TableColumn<Wildfire, String> wildfireLatitudeCol;
    @FXML private TableColumn<Wildfire, String> wildfireLongitudeCol;
    @FXML private TableColumn<Wildfire, String> wildfireRadiusCol;
    @FXML private TableColumn<Wildfire, String> redZoneTextCol;

    MapTabController mapTabController;
    @FXML private Button showWildfiresButton;

    FirestationsTabController firestationsTabController;
    @FXML private Button searchStationsButton;
    @FXML private TableView<Firestation> firestationTable;
    @FXML public TableColumn<Firestation, String> firestationNameCol;
    @FXML public TableColumn<Firestation, String> firestationLatitudeCol;
    @FXML public TableColumn<Firestation, String> firestationLongitudeCol;
    @FXML public TableColumn<Firestation, String> firestationPhoneCol;


    PeopleInAreaTabController peopleInAreaTabController;
    @FXML private Button searchPeopleButton;
    @FXML private TableView<Person> peopleTable;
    @FXML private TableColumn<Person, String> personIdCol;
    @FXML private TableColumn<Person, String> lastNameCol;
    @FXML private TableColumn<Person, String> districtCol;
    @FXML private TableColumn<Person, String> cityCol;
    @FXML private TableColumn<Person, String> streetCol;
    @FXML private TableColumn<Person, String> personLatitudeCol;
    @FXML private TableColumn<Person, String> personLongitudeCol;
    @FXML private TableColumn<Person, String> phoneCol;

    SelectAFireOfWildfiresController selectAFireOfWildfiresController;
    //@FXML private Button searchPeopleRedZoneButton;
    @FXML private TableView<Person> peopleRedZoneTable;
    @FXML private TableColumn<Person, String> personRedZoneIdCol;
    @FXML private TableColumn<Person, String> personRedZoneLastnameCol;
    @FXML private TableColumn<Person, String> personRedZoneDistrictCol;
    @FXML private TableColumn<Person, String> personRedZoneCityCol;
    @FXML private TableColumn<Person, String> personRedZoneStreetCol;
    @FXML private TableColumn<Person, String> personRedZonePhoneCol;
    @FXML private TableColumn<Person, String> personRedZoneLatitudeCol;
    @FXML private TableColumn<Person, String> personRedZoneLongitudeCol;

    @FXML private Tab weatherTab;
    @FXML private TableView<WeatherList> weatherTable;
    @FXML private TableColumn<WeatherList, String> weatherIdCol;
    @FXML private TableColumn<WeatherList, String> weatherDateTimeCol;
    @FXML private TableColumn<WeatherList, String> weatherTempCol;
    @FXML private TableColumn<WeatherList, String> weatherWindSpeedCol;
    @FXML private TableColumn<WeatherList, String> weatherWindDirectionCol;
    @FXML private TableColumn<WeatherList, String> weatherHumidityCol;
    @FXML private Button getWeatherButton;


    WebViewController webViewController;
    @FXML private WebView mapWebView;
    private WebEngine webEngine;
    private Wildfire selectedFire;

    WeatherTabController weatherConditions;

    MapTabUpdateButtonController mapTabUpdateButtonController;
    @FXML private Button updateWildfireButton;

    MapTabDeleteButtonController mapTabDeleteButtonController;
    @FXML private Button deleteWildfireButton;

    RescuersInAreaTabController rescuersInAreaTabController;
    @FXML private TableView<Rescuer> rescuersInAreaTable;
    @FXML private Button addRescuersInAreaButton;
    @FXML private TableColumn <Rescuer, String> rescuerIdCol;
    @FXML private TableColumn <Rescuer, String> rescuerNameCol;
    @FXML private TableColumn <Rescuer, String> rescuerLastnameCol;
    @FXML private TableColumn<Rescuer, String> rescuerHomeCityCol;
    @FXML private TableColumn<Rescuer, String> rescuerStationCol;
    @FXML private Label wildfireNameInRescuersLabel;
    @FXML private Label noRescuersAddedLabel;
    @FXML private Label weatherPlaceNameLabel;

    private ButtonAddRescuersInAreaController buttonAddRescuersInAreaController;

    public AppController(){}
    public AppController(SessionFactory session) {
        this.session = session;
    }

    public void initialize() throws IOException {
        initializeWildfireView();
        initializeMapTabWildfiresTableView();
        initializeFirestationView();
        initializePeopleInAreaView();
        initializeSelectAFireOfWildfires();
        initializeMapViewUpdate();
        initializeMapViewDelete();
        initializeAddRescuersSceneCreation();
    }

    public void initializeAddRescuersSceneCreation(){
        buttonAddRescuersInAreaController = new ButtonAddRescuersInAreaController(session);
        buttonAddRescuersInAreaController.setButton(addRescuersInAreaButton);
        buttonAddRescuersInAreaController.setWildfireTable(wildfireTable);
    }
    public void initializeMapViewDelete(){
        mapTabDeleteButtonController = new MapTabDeleteButtonController(session);
        mapTabDeleteButtonController.setButton(deleteWildfireButton);
        mapTabDeleteButtonController.setWildfireTable(wildfireTable);
    }
    public void initializeMapViewUpdate(){
        mapTabUpdateButtonController = new MapTabUpdateButtonController(session);
        mapTabUpdateButtonController.setButton(updateWildfireButton);
        mapTabUpdateButtonController.setWildfireTable(wildfireTable);
    }
    public void initializeWildfireView(){
        //initialize all FXML instances: buttons, labels etc
        wildfireTabController = new WildfireTabController(session);
        wildfireTabController.setButton(addLocationButton);
        wildfireTabController.setLatitudeText(latitudeText);
        wildfireTabController.setLongitudeText(longitudeText);
        wildfireTabController.setRadiusText(radiusText);
        wildfireTabController.setRedZoneCoefficientText(redZoneCoefficientText);
        wildfireTabController.setErrorLabel(errorLabel);
        wildfireTabController.setFireNameText(fireNameText);

        wildfireTabController.setWildfireTable(wildfireTable);
        wildfireTabController.setWildfireIdCol(wildfireIdCol);
        wildfireTabController.setWildfireNameCol(wildfireNameCol);
        wildfireTabController.setWildfireLatitudeCol(wildfireLatitudeCol);
        wildfireTabController.setWildfireLongitudeCol(wildfireLongitudeCol);
        wildfireTabController.setWildfireRadiusCol(wildfireRadiusCol);
        wildfireTabController.setRedZoneTextCol(redZoneTextCol);
    }
    public void initializeMapTabWildfiresTableView(){
        mapTabController = new MapTabController(session);
        mapTabController.setButton(showWildfiresButton);
        mapTabController.setWildfireIdCol(wildfireIdCol);
        mapTabController.setWildfireNameCol(wildfireNameCol);
        mapTabController.setWildfireTable(wildfireTable);
        mapTabController.setWildfireLatitudeCol(wildfireLatitudeCol);
        mapTabController.setWildfireLongitudeCol(wildfireLongitudeCol);
        mapTabController.setWildfireRadiusCol(wildfireRadiusCol);
        mapTabController.setRedZoneTextCol(redZoneTextCol);
    }
    public void initializeFirestationView(){
        firestationsTabController = new FirestationsTabController(session);
        firestationsTabController.setFirestationTable(firestationTable);
        firestationsTabController.setButton(searchStationsButton);
        firestationsTabController.setFirestationNameCol(firestationNameCol);
        firestationsTabController.setFirestationLatitudeCol(firestationLatitudeCol);
        firestationsTabController.setFirestationLongitudeCol(firestationLongitudeCol);
        firestationsTabController.setFirestationPhoneCol(firestationPhoneCol);
    }

    public void initializePeopleInAreaView() {
        peopleInAreaTabController = new PeopleInAreaTabController(session);
        peopleInAreaTabController.setPeopleTable(peopleTable);
        peopleInAreaTabController.setButton(searchPeopleButton);
        peopleInAreaTabController.setPersonIdCol(personIdCol);
        peopleInAreaTabController.setLastNameCol(lastNameCol);
        peopleInAreaTabController.setDistrictCol(districtCol);
        peopleInAreaTabController.setCityCol(cityCol);
        peopleInAreaTabController.setStreetCol(streetCol);
        peopleInAreaTabController.setPersonLatitudeCol(personLatitudeCol);
        peopleInAreaTabController.setPersonLongitudeCol(personLongitudeCol);
    }

    public void initializeSelectAFireOfWildfires() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("target\\classes\\app.properties"));
        selectAFireOfWildfiresController = new SelectAFireOfWildfiresController(session, properties);
        selectAFireOfWildfiresController.setMapWebView(mapWebView);;

        selectAFireOfWildfiresController.setPeopleRedZoneTable(peopleRedZoneTable);
        selectAFireOfWildfiresController.setPersonRedZoneIdCol(personRedZoneIdCol);
        selectAFireOfWildfiresController.setPersonRedZoneLastnameCol(personRedZoneLastnameCol);
        selectAFireOfWildfiresController.setPersonRedZoneDistrictCol(personRedZoneDistrictCol);
        selectAFireOfWildfiresController.setPersonRedZoneCityCol(personRedZoneCityCol);
        selectAFireOfWildfiresController.setPersonRedZoneStreetCol(personRedZoneStreetCol);
        selectAFireOfWildfiresController.setPersonRedZonePhoneCol(personRedZonePhoneCol);
        selectAFireOfWildfiresController.setPersonRedZoneLatitudeCol(personRedZoneLatitudeCol);
        selectAFireOfWildfiresController.setPersonRedZoneLongitudeCol(personRedZoneLongitudeCol);

        selectAFireOfWildfiresController.setGetWeatherButton(getWeatherButton);
        selectAFireOfWildfiresController.setWeatherTable(weatherTable);
        selectAFireOfWildfiresController.setWeatherTab(weatherTab);
        selectAFireOfWildfiresController.setWeatherDateTimeCol(weatherDateTimeCol);
        selectAFireOfWildfiresController.setWeatherTempCol(weatherTempCol);
        selectAFireOfWildfiresController.setWeatherWindSpeedCol(weatherWindSpeedCol);
        selectAFireOfWildfiresController.setWeatherWindDirectionCol(weatherWindDirectionCol);
        selectAFireOfWildfiresController.setWeatherHumidityCol(weatherHumidityCol);
        selectAFireOfWildfiresController.setWeatherPlaceNameLabel(weatherPlaceNameLabel);

        selectAFireOfWildfiresController.setWildfireTable(wildfireTable);
        selectAFireOfWildfiresController.onWildfireTableClick(wildfireTable);

        initializeShowRescuersInAreaView();

    }
    public void initializeShowRescuersInAreaView(){
        selectAFireOfWildfiresController.setAddRescuersInAreaButton(addRescuersInAreaButton);
        selectAFireOfWildfiresController.setRescuersInAreaTable(rescuersInAreaTable);
        selectAFireOfWildfiresController.setRescuerIdCol(rescuerIdCol);
        selectAFireOfWildfiresController.setRescuerNameCol(rescuerNameCol);
        selectAFireOfWildfiresController.setRescuerLastnameCol(rescuerLastnameCol);
        selectAFireOfWildfiresController.setRescuerHomeCityCol(rescuerHomeCityCol);
        selectAFireOfWildfiresController.setRescuerStationCol(rescuerStationCol);
        selectAFireOfWildfiresController.setWildfireNameInRescuersLabel(wildfireNameInRescuersLabel);
        selectAFireOfWildfiresController.setNoRescuersAddedLabel(noRescuersAddedLabel);

    }
}


