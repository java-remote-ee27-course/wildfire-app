package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.DataSingleton;
import com.lunamezzogiorno.wildfireapp.WildfireApp;
import com.lunamezzogiorno.wildfireapp.data.dao.*;
import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Radius;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * The controller that controls the actions during updating the wildfire data on Map tab.
 * To update wildfire data, a new window is opened on top of the main window -
 * this controller controls the actions that happen in that window (new scene):
 * 1) letting to update wildfire data and 2) closing the window after pressing "Update" button.
 * The wildfire data is fetched from another scene by DataSingleton class instance.
 * The code has two bigger parts:
 * 1) initializing text fields with the selected wildfire data from Map tab table.
 * 2) creating logic and actions connected to Update button:
 * 2.1) fetching the data to update,
 * 2.2) updating the selected wildfire data in database,
 * 2.3) closing the scene, giving control back to the main scene.
 *
 * The idea of the singleton data exchange is taken from:
 * https://www.youtube.com/watch?v=HJC_JxpHTeU&ab_channel=CoolITHelp
 *
 * TODO:: DRY my code, especially Controller classes
 **
 ** TODO:: Wf Radius is not updated if it already exists in DB - see why
 */
@AllArgsConstructor
@Getter
@Setter
public class UpdateWildfireViewController implements Initializable {

    @FXML private TextField updateWildfireNameText;
    @FXML private TextField updateWildfireLatitudeText;
    @FXML private TextField updateWildfireLongitudeText;
    @FXML private TextField updateWildfireRadiusText;
    @FXML private TextField updateRedZoneCoefficientText;
    @FXML private Button updateWildfireViewButton;
    @FXML private Label updateErrorLabel;
    @FXML private GridPane updateViewGridPane;

    private SessionFactory session;
    private Radius radius;

    private Location location;
    private Wildfire wildfire;

    private LocationDao locationDaoObject;
    private WildfireDao wildfireDaoObject;
    private RadiusDao radiusDaoObject;

    private Double [] radiusObjectRadius;
    private Double [] locationCoords;
    private Object [] wildfireInfo;
    private Boolean isDouble = true;

    MapTabUpdateButtonController mapTabUpdateButtonController;
    @FXML private Button updateWildfireButton;

    DataSingleton data = DataSingleton.getInstance();

    public UpdateWildfireViewController() { }

    public UpdateWildfireViewController(SessionFactory session) {
        this.session = session;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        mapTabUpdateButtonController = new MapTabUpdateButtonController(session);
        wildfire = data.getWildfire();
        updateWildfireNameText.setText(wildfire.getName());
        showInitialWildfireInfo();
        setButton(updateWildfireButton);
    }

    //initial text in textFields (selected wildfire's info)
    //See https://www.youtube.com/watch?v=MsgiJdf5njc&ab_channel=Randomcode
    public void showInitialWildfireInfo(){
        location = wildfire.getLocation();
        radius = wildfire.getRadius();

        updateWildfireNameText.setText(wildfire.getName());
        updateWildfireLatitudeText.setText(String.valueOf(location.getLatitude()));
        updateWildfireLongitudeText.setText(String.valueOf(location.getLongitude()));
        updateWildfireRadiusText.setText(String.valueOf(radius.getRadius()));
        updateRedZoneCoefficientText.setText(String.valueOf(wildfire.getRedZoneCoefficient()));
    }


    /********* NEXT SECTION IS CONNECTED TO THE ACTIONS OF THE UPDATE BUTTON: ***********/

    public void setButton(Button button){
        updateWildfireViewButton.setOnAction(actionEvent -> {
            var ses = session.openSession();

            try (ses) {
                var tx = ses.beginTransaction();
                locationDaoObject = new LocationDao(ses);
                wildfireDaoObject = new WildfireDao(ses);

                radiusDaoObject = new RadiusDao(ses);

                updateErrorLabel.setText("");
                getUpdatedWildfireValuesFromTextFields();
                updateWildfireInfo();
                //emptyInputTextFields();
                goBackToMainWindow(actionEvent);
                tx.commit();
            } catch (NumberFormatException | IOException e) {
                if (e.getClass().getSimpleName().equals("NumberFormatException"))
                    updateErrorLabel.setText("Radius, location, coefficient must be numbers!");
                System.out.println(
                        e.getCause() + ", " +
                                e.getMessage() + " " +
                                e.getClass() + "\n-----\n" +
                                this.getClass() + ", " +
                                "line number: " +
                                e.getStackTrace()[3].getLineNumber() + ", " +
                                e.getStackTrace()[0].getMethodName() + " " +
                                e.getStackTrace()[1].getMethodName() + " " +
                                e.getStackTrace()[2].getMethodName() + " " +
                                ": Updatable radius, location and coefficient must be doubles!");
            }
        });
    }

    public void getUpdatedWildfireValuesFromTextFields(){
        radiusObjectRadius = new Double[]{getRadiusDoubleFromTextField()};
        locationCoords = new Double[]{getLatitudeDoubleFromTextField(), getLongitudeDoubleFromTextField()};
        wildfireInfo = new Object[]{getWildfireNameFromTextField(), getRedZoneAreaFromTextField()};
    }

    public void goBackToMainWindow(ActionEvent event) throws IOException {
        Stage stage = (Stage) this.updateWildfireViewButton.getScene().getWindow();
        var resource = WildfireApp.class.getResource("wildfireapp-view.fxml");
        var fxmlLoader = new FXMLLoader(resource);
        var appController = new AppController(session);
        fxmlLoader.setController(appController);
        Parent root = fxmlLoader.load();
        stage.setTitle("Wildfire app");
        stage.setScene(new Scene(root, 650, 800));
    }

    public void updateWildfireInfo(){
            checkWildfireFromDatabaseAndSetNewEntityValues();
            updateDaoObject(radiusDaoObject, radius, radiusObjectRadius);
            updateDaoObject(locationDaoObject, location, locationCoords);
            updateDaoObject(wildfireDaoObject, wildfire, wildfireInfo);

    }

    public void checkWildfireFromDatabaseAndSetNewEntityValues(){
        //Check: if radius value is present in db then use it, otherwise use existing.
        //If you check it after object is created, it becomes transient w/o saving to DB

        if(isObjectPresentInDatabase(radiusDaoObject, radiusObjectRadius))
            getObjectFromDatabase(radiusDaoObject, radiusObjectRadius)
                    .ifPresent(r -> radius = r);
        else {
            radius.setRadius(getRadiusDoubleFromTextField());
        }

        if(isObjectPresentInDatabase(locationDaoObject, locationCoords)){
            getObjectFromDatabase(locationDaoObject, locationCoords)
                    .ifPresent(l -> location = l);
        }
        else {
            location.setLatitude(getLatitudeDoubleFromTextField());
            location.setLongitude(getLongitudeDoubleFromTextField());
        }

        wildfire.setRadius(radius);
        wildfire.setLocation(location);
        wildfire.setRedZoneCoefficient(getRedZoneAreaFromTextField());
        wildfire.setName(getWildfireNameFromTextField());
    }


    public void checkIfDoubleTextFieldIsBlank(){
        //check if field value is blank, if so, convert it to "0.0"
        List<TextField> textFields = new ArrayList<>(List.of(
                updateWildfireNameText,
                updateWildfireLatitudeText,
                updateWildfireLongitudeText,
                updateWildfireRadiusText,
                updateRedZoneCoefficientText));

        for(TextField t : textFields) {
            if (!(t == updateWildfireNameText) && (t.getText().isBlank() || t.getText().isEmpty())) t.setText("0.0");
        }
    }

    // if obj with parameters value[] is present in DB, get existing values,
    // do not create a new row with duplicate value to DB:
    private <T> Boolean isObjectPresentInDatabase(EvaluativeDao<T> t, Double[] value){
        return t.isPresent(value);
    }

    //retrieving an object from DB by its field value(s) (e.g, by radius or by lat+lon)
    private <T, V> Optional<T> getObjectFromDatabase(SelectableDao<T, V> t, V [] value){
        return t.get(value);
    }

    private <T, V> void updateDaoObject(UpdatableDao<T, V> u, T t, V[] v){
        u.update(t, v);
    }

    //convert text field value to Double
    private Double getRadiusDoubleFromTextField(){
        checkIfDoubleTextFieldIsBlank();
        return Double.parseDouble(updateWildfireRadiusText.getText());
    }
    private Double getLatitudeDoubleFromTextField(){
        checkIfDoubleTextFieldIsBlank();
        return Double.parseDouble(updateWildfireLatitudeText.getText());
    }

    private Double getLongitudeDoubleFromTextField(){
        checkIfDoubleTextFieldIsBlank();
        return Double.parseDouble(updateWildfireLongitudeText.getText());
    }

    private Double getRedZoneAreaFromTextField(){
        checkIfDoubleTextFieldIsBlank();
        return Double.parseDouble(updateRedZoneCoefficientText.getText());
    }

    private String getWildfireNameFromTextField(){
        return updateWildfireNameText.getText();
    }

}


