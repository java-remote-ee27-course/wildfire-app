package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.*;
import com.lunamezzogiorno.wildfireapp.data.dao.PersonDao;
import com.lunamezzogiorno.wildfireapp.data.dao.SelectableDao;
import com.lunamezzogiorno.wildfireapp.data.dao.WildfireDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.*;
import org.hibernate.SessionFactory;

import java.util.List;


/**
 * TODO:: implement people search, which searches the persons in "Red Area" of a particular wildfire
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PeopleInAreaTabController {
    @FXML private Button searchPeopleButton;
    @FXML private TableView<Person> peopleTable;
    @FXML private TableColumn<Person, String> personIdCol;
    @FXML private TableColumn<Person, String> lastNameCol;
    @FXML private TableColumn<Person, String> districtCol;
    @FXML private TableColumn<Person, String> cityCol;
    @FXML private TableColumn<Person, String> streetCol;
    @FXML private TableColumn<Person, String> personLatitudeCol;
    @FXML private TableColumn<Person, String> personLongitudeCol;
    @FXML private TableColumn<Person, String> phoneCol;

    private SessionFactory session;
    private Address address;
    private PersonDao people;
    private RedZoneCalculations redZoneCalculations;
    private Wildfire wildfire;
    private WildfireDao wildfireDao;


    public PeopleInAreaTabController(SessionFactory session) {
        this.session = session;
    }

    public void setButton(Button button){
        this.searchPeopleButton = button;

        searchPeopleButton.setOnAction(actionEvent -> {
            emptyPersonsTableFields();
            var ses = session.openSession();
            var tx = ses.beginTransaction();
            people = new PersonDao(ses);

            showAllPeopleInApp();

            tx.commit();
            ses.close();
        });
    }


    public void emptyPersonsTableFields(){
        persons.clear();
    }
    public <T, V> List<T> getAllPeople(SelectableDao<T, V> s){
        return s.getAll();
    }

    private ObservableList<Person> persons = FXCollections.observableArrayList();

    //add all rows from DB to the Javafx People gridtable on "People in Area" tab:
    public void showAllPeopleInApp(){
        persons.addAll(getAllPeople(people));
        personIdCol.setCellValueFactory(new PropertyValueFactory<>("Id"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("Lastname"));
        districtCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getDistrict()));
        cityCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getCity()));
        streetCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getStreet()));
        personLatitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getLocation()
                .getLatitude()
                .toString()));
        personLongitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getAddress()
                .getLocation()
                .getLongitude()
                .toString()));

        peopleTable.setItems(persons);
    }

}
