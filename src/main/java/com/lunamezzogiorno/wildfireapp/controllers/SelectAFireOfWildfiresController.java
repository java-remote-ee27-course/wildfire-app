package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.DataSingleton;
import com.lunamezzogiorno.wildfireapp.data.*;
import com.lunamezzogiorno.wildfireapp.data.dao.*;
import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import com.lunamezzogiorno.wildfireapp.data.entities.Rescuer;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import com.lunamezzogiorno.wildfireapp.weatherdata.WeatherList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.SessionFactory;

import java.util.Properties;

/**
 * TODO::Add info to this tab, which wildfire and what city are displayed
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectAFireOfWildfiresController {
    @FXML private TableView<Person> peopleRedZoneTable;
    @FXML private TableColumn<Person, String> personRedZoneIdCol;
    @FXML private TableColumn<Person, String> personRedZoneLastnameCol;
    @FXML private TableColumn<Person, String> personRedZoneDistrictCol;
    @FXML private TableColumn<Person, String> personRedZoneCityCol;
    @FXML private TableColumn<Person, String> personRedZoneStreetCol;
    @FXML private TableColumn<Person, String> personRedZonePhoneCol;
    @FXML private TableColumn<Person, String> personRedZoneLatitudeCol;
    @FXML private TableColumn<Person, String> personRedZoneLongitudeCol;
    private SessionFactory session;
    private PersonDao people;
    private RedZoneCalculations redZoneCalculations;

    @FXML private TableView<Rescuer> rescuersInAreaTable;
    @FXML private Button addRescuersInAreaButton;
    @FXML private TableColumn <Rescuer, String> rescuerIdCol;
    @FXML private TableColumn <Rescuer, String> rescuerNameCol;
    @FXML private TableColumn <Rescuer, String> rescuerLastnameCol;
    @FXML private TableColumn<Rescuer, String> rescuerHomeCityCol;
    @FXML private TableColumn<Rescuer, String> rescuerStationCol;
    @FXML private Label wildfireNameInRescuersLabel;
    @FXML private Label noRescuersAddedLabel;
    private RescuerDao rescuerDao;
    private FirestationDao firestationDao;

    @FXML private TableView <Wildfire> wildfireTable;
    @FXML private WebView mapWebView;
    private Wildfire wildfire;
    private Double latitude, longitude;
    private WildfireDao wildfireDao;

    @FXML private Tab weatherTab;
    @FXML private TableView<WeatherList> weatherTable;
    @FXML private TableColumn<WeatherList, String> weatherIdCol;
    @FXML private TableColumn<WeatherList, String> weatherDateTimeCol;
    @FXML private TableColumn<WeatherList, String> weatherTempCol;
    @FXML private TableColumn<WeatherList, String> weatherWindSpeedCol;
    @FXML private TableColumn<WeatherList, String> weatherWindDirectionCol;
    @FXML private TableColumn<WeatherList, String> weatherHumidityCol;
    @FXML private Label weatherPlaceNameLabel;
    @FXML private Button getWeatherButton;
    private Properties properties;


    MapTabUpdateButtonController mapTabUpdateButtonController;
    @FXML private Button updateWildfireButton;

    DataSingleton data = DataSingleton.getInstance();

    public SelectAFireOfWildfiresController(SessionFactory session, Properties properties) {
        this.session = session;
        this.properties = properties;
    }

    public void onWildfireTableClick(TableView<Wildfire> wildfireTable){
        this.wildfireTable = wildfireTable;

        wildfireTable.setOnMouseClicked((MouseEvent e) -> {
            var ses = session.openSession();
            var tx = ses.beginTransaction();
            people = new PersonDao(ses);
            rescuerDao = new RescuerDao(ses);
            firestationDao = new FirestationDao(ses);
            try {
                final Wildfire[] wildfireData = new Wildfire[1];
                if (e.getButton() == MouseButton.PRIMARY || e.getButton() == MouseButton.SECONDARY) {

                    wildfireData[0] = selectWildfireAndShowData(wildfireTable);

                    Object[] fireObject = new Long[]{wildfireData[0].getId()};

                    //Set wildfire:
                    wildfireDao = new WildfireDao(ses);
                    var mapTabController = new MapTabController();
                    mapTabController.setWildfireTable(wildfireTable);
                    wildfire = wildfireDao.get(fireObject).orElse(null); //Get wildfire by id to display info.

                    //show residents in red zone of the selected wildfire:
                    var residentsInRedZoneController = new ResidentsInRedZoneTabController();
                    setAndShowResidentsInRedZoneInApp(residentsInRedZoneController);

                    var rescuersInAreaTabController = new RescuersInAreaTabController(session);
                    setAndShowRescuersInArea(rescuersInAreaTabController);

                    //Show map (from web) of the selected wildfire:
                    latitude = wildfire.getLocation().getLatitude();
                    longitude = wildfire.getLocation().getLongitude();

                    var mapWebView = new WebViewController();
                    setAndShowMapWebViewInApp(mapWebView);

                    //Show weather data of the selected wildfire:
                    var weatherTabController = new WeatherTabController();
                    setAndShowWeatherInApp(weatherTabController);

                    //Show selected wildfire info in wildfire update view:
                    data.setWildfire(wildfire);

                }

                tx.commit();
            } catch(NullPointerException ex) {
                System.out.println("No data was selected in the table " +
                        "or a value was not found in DB");
                System.out.println(ex + ", " +
                        ex.getMessage() + ", " +
                        ex.getStackTrace()[0].getMethodName() +
                        " in rows: " + ex.getStackTrace()[0].getLineNumber() +
                        ", " + ex.getStackTrace()[1].getLineNumber() +
                        ", " + ex.getStackTrace()[2].getLineNumber() +
                        ", " + ex.getStackTrace()[3].getLineNumber() +
                        " in class: " +
                        this.getClass() +
                        " or in the class that it calls");
            }
            finally{
                ses.close();
            }
        });
    }


    public Wildfire selectWildfireAndShowData(TableView<Wildfire> wildfireTable) {
        this.wildfireTable = wildfireTable;
        final Wildfire[] wildfire = new Wildfire[1];

        if (wildfireTable.getSelectionModel().getSelectedItem() != null) {
            wildfire[0] = wildfireTable.getSelectionModel().getSelectedItem();
        }
        return wildfire[0];
    }

    public void setAndShowResidentsInRedZoneInApp(ResidentsInRedZoneTabController peopleInAreaRedZoneController){
        peopleInAreaRedZoneController.setPeopleRedZoneTable(peopleRedZoneTable);
        peopleInAreaRedZoneController.setPersonRedZoneIdCol(personRedZoneIdCol);
        peopleInAreaRedZoneController.setPersonRedZoneLastnameCol(personRedZoneLastnameCol);
        peopleInAreaRedZoneController.setPersonRedZoneDistrictCol(personRedZoneDistrictCol);
        peopleInAreaRedZoneController.setPersonRedZoneCityCol(personRedZoneCityCol);
        peopleInAreaRedZoneController.setPersonRedZoneStreetCol(personRedZoneStreetCol);
        peopleInAreaRedZoneController.setPersonRedZonePhoneCol(personRedZonePhoneCol);
        peopleInAreaRedZoneController.setPersonRedZoneLatitudeCol(personRedZoneLatitudeCol);
        peopleInAreaRedZoneController.setPersonRedZoneLongitudeCol(personRedZoneLongitudeCol);
        peopleInAreaRedZoneController.setPeople(people);
        peopleInAreaRedZoneController.setWildfire(wildfire);
        peopleInAreaRedZoneController.showResidentsInRedZoneInApp();
    }

    public void setAndShowRescuersInArea(RescuersInAreaTabController rescuersInAreaTabController){

        rescuersInAreaTabController.setWildfireNameInRescuersLabel(wildfireNameInRescuersLabel);
        rescuersInAreaTabController.setWildfire(wildfire);
        rescuersInAreaTabController.displayWildfireName(wildfire);
        rescuersInAreaTabController.setRescuerDao(rescuerDao);
        rescuersInAreaTabController.setFirestationDao(firestationDao);
        rescuersInAreaTabController.setWildfires(wildfireDao);

        rescuersInAreaTabController.setButton(addRescuersInAreaButton);
        rescuersInAreaTabController.setRescuersInAreaTable(rescuersInAreaTable);
        rescuersInAreaTabController.setRescuerIdCol(rescuerIdCol);
        rescuersInAreaTabController.setRescuerNameCol(rescuerNameCol);
        rescuersInAreaTabController.setRescuerLastnameCol(rescuerLastnameCol);
        rescuersInAreaTabController.setRescuerHomeCityCol(rescuerHomeCityCol);
        rescuersInAreaTabController.setRescuerStationCol(rescuerStationCol);
        rescuersInAreaTabController.setNoRescuersAddedLabel(noRescuersAddedLabel);


        rescuersInAreaTabController.showAllRescuersInApp();
    }

    public void setAndShowMapWebViewInApp(WebViewController mapWebView){
        mapWebView.setMapWebView(this.mapWebView);
        mapWebView.loadPage(latitude, longitude);
    }

    public void setAndShowWeatherInApp(WeatherTabController weatherConditions){
        weatherConditions.setLatitude(latitude);
        weatherConditions.setLongitude(longitude);
        weatherConditions.setProps(properties);
        weatherConditions.setButton(getWeatherButton);
        weatherConditions.setWeatherTable(weatherTable);
        weatherConditions.setWeatherTab(weatherTab);
        weatherConditions.setWeatherDateTimeCol(weatherDateTimeCol);
        weatherConditions.setWeatherTempCol(weatherTempCol);
        weatherConditions.setWeatherWindSpeedCol(weatherWindSpeedCol);
        weatherConditions.setWeatherWindDirectionCol(weatherWindDirectionCol);
        weatherConditions.setWeatherHumidityCol(weatherHumidityCol);
        weatherConditions.setWeatherPlaceNameLabel(weatherPlaceNameLabel);
        weatherConditions.displayWildfireNameInApp(wildfire);
    }
}

