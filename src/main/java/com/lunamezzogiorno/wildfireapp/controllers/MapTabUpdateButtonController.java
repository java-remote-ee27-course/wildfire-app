package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.WildfireApp;
import com.lunamezzogiorno.wildfireapp.data.dao.WildfireDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import lombok.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionDelegatorBaseImpl;

import java.io.IOException;
import java.util.Optional;

/**
 * Controller that creates new scene for updating wildfire data on Map tab.
 */

@AllArgsConstructor
@Getter
@Setter
public class MapTabUpdateButtonController{
    private SessionFactory session;
    private Wildfire wildfire;
    @FXML private Button updateWildfireButton;
    @FXML private TableView<Wildfire> wildfireTable;

    public MapTabUpdateButtonController(){}
    public MapTabUpdateButtonController(SessionFactory session) {
        this.session = session;
    }

    //See example: https://www.youtube.com/watch?v=MsgiJdf5njc&ab_channel=Randomcode
    public void setButton(Button button){
        this.updateWildfireButton = button;
        this.updateWildfireButton.setOnMouseClicked(mouseEvent -> {

            var stage = (Stage) this.updateWildfireButton.getScene().getWindow();
            var resource = WildfireApp.class.getResource("wildfireupdate-view.fxml");
            var fxmlLoader = new FXMLLoader(resource);
            var updateWildfireViewController = new UpdateWildfireViewController(session);
            fxmlLoader.setController(updateWildfireViewController);
            getSelectedFireAndCreateScene(fxmlLoader, stage);
        });
    }

    //The optional isFireSelected() checks if a fire is selected.
    //If not, the NullpointerException is catched, the scene will not be created
    //and the Alert window is shown.
    public void getSelectedFireAndCreateScene(FXMLLoader fxmlLoader, Stage stage){
        try {
            if(isAFireSelected().isPresent())
                createUpdateWildfireScene(fxmlLoader, stage);

        } catch(NullPointerException e){
                Alert alert = new Alert(Alert.AlertType.NONE);
                ButtonType type = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
                alert.getDialogPane().getButtonTypes().add(type);
                alert.setContentText("\nTo update wildfire info, please select a wildfire first!\n\n");
                alert.setTitle("Alert");
                alert.showAndWait();
                System.out.println(
                    e.getCause() + ", " +
                            e.getMessage() + " " +
                            e.getClass() + "\n-----\n" +
                            "No wildfire is selected in the Map tab to update it.\n" +
                            "Please select a wildfire first before pressing UPDATE!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public Optional<Wildfire> isAFireSelected(){
        return Optional.of(wildfireTable.getSelectionModel().getSelectedItem());
    }

    public void createUpdateWildfireScene(FXMLLoader fxmlLoader, Stage stage) throws IOException {
        Parent root = fxmlLoader.load();
        stage.setTitle("Update wildfire info");
        stage.setScene(new Scene(root, 650, 700));
    }
}
