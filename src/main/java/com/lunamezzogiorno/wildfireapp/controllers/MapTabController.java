package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.*;
import com.lunamezzogiorno.wildfireapp.data.dao.*;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@Getter
@Setter
public class MapTabController {
    @FXML private Tab mapTab;
    @FXML private TableView<Wildfire> wildfireTable;
    @FXML private TableColumn<Wildfire, String> wildfireIdCol;
    @FXML private TableColumn<Wildfire, String> wildfireNameCol;
    @FXML private TableColumn<Wildfire, String> wildfireLatitudeCol;
    @FXML private TableColumn<Wildfire, String> wildfireLongitudeCol;
    @FXML private TableColumn<Wildfire, String> wildfireRadiusCol;
    @FXML private TableColumn<Wildfire, String> redZoneTextCol;
    @FXML private Button showWildfiresButton;

    private SessionFactory session;
    private WildfireDao wildfires;


    private ObservableList<Wildfire> fires = FXCollections.observableArrayList();
    private RedZoneCalculations redZoneCalculations;

    public MapTabController(){}
    public MapTabController(SessionFactory session) {
        this.session = session;
    }
    public void setButton(Button button) {
        this.showWildfiresButton = button;

        showWildfiresButton.setOnAction(actionEvent -> {
            emptyWildfireTableFields();
            var ses = session.openSession();
            var tx = ses.beginTransaction();
            wildfires = new WildfireDao(ses);

            showAllWildfiresInApp();

            tx.commit();
            ses.close();

        });
    }

    //add all rows from DB to the Javafx Wildfire gridtable on "Wildfire" tab:
    public void showAllWildfiresInApp(){
        fires.addAll(getAllObjectsFromDatabase(wildfires));
        wildfireIdCol.setCellValueFactory(new PropertyValueFactory<>("Id"));
        wildfireNameCol.setCellValueFactory(new PropertyValueFactory<>("Name"));
        wildfireLatitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getLocation()
                .getLatitude()
                .toString()));
        wildfireLongitudeCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getLocation()
                .getLongitude()
                .toString()));

        wildfireRadiusCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data
                .getValue()
                .getRadius()
                .getRadius()
                .toString()));

        //Red Zone radius column:
        redZoneTextCol.setCellValueFactory(data ->
                new ReadOnlyStringWrapper(getRedZoneRadii()
                        .stream()
                        .map(Objects::toString)
                        .toList()
                        .get(fires.indexOf(data.getValue()))));
        wildfireTable.setItems(fires);

    }

    public <T, V> List<T> getAllObjectsFromDatabase(SelectableDao<T, V> s){
        return s.getAll();
    }


    public List<Double> getRedZoneRadii(){
        return fires
                .stream()
                .map(f -> new RedZoneCalculations(f).getRedZoneRadius())
                .toList();
    }


    public void emptyWildfireTableFields(){
        fires.clear();
    }

}
