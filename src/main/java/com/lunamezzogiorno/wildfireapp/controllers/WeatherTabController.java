package com.lunamezzogiorno.wildfireapp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lunamezzogiorno.wildfireapp.DataSingleton;
import com.lunamezzogiorno.wildfireapp.data.dao.WeatherDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import com.lunamezzogiorno.wildfireapp.weatherdata.*;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.Properties;


/**
 * Weather info is fetched from web, fom OpenWeatherMap. If you clone the repository,
 * to make things work, you would need to get a OpenWeatherMAp API key for "Call 5 day / 3 hour forecast data":
 * https://openweathermap.org/forecast5#geo5
 *
 * For myself as a reminder for the future: to resolve error "Caused by: java.lang.reflect.InaccessibleObjectException:
 * Unable to make public com.lunamezzogiorno.wildfireapp.weatherdata.WeatherRoot() accessible", see:
 * https://stackoverflow.com/questions/76624671/java-lang-reflect-inaccessibleobjectexception-unable-to-make-public-accessible
 * https://openjdk.org/projects/jigsaw/quick-start
 * TODO::Add info to this tab which wildfire and what city are displayed
 */

@Getter
@Setter
public class WeatherTabController {
    @FXML private Tab weatherTab;
    @FXML private TableView<WeatherList> weatherTable;
    @FXML private TableColumn<WeatherList, String> weatherIdCol;
    @FXML private TableColumn<WeatherList, String> weatherDateTimeCol;
    @FXML private TableColumn<WeatherList, String> weatherTempCol;
    @FXML private TableColumn<WeatherList, String> weatherWindSpeedCol;
    @FXML private TableColumn<WeatherList, String> weatherWindDirectionCol;
    @FXML private TableColumn<WeatherList, String> weatherHumidityCol;
    @FXML private Label weatherPlaceNameLabel;
    @FXML private Button getWeatherButton;

    WeatherRoot weatherRoot;
    private ObservableList<WeatherList> weatherLists = FXCollections.observableArrayList();
    private Properties props;
    private Double latitude;
    private Double longitude;

    public WeatherTabController() {}

    public WeatherTabController(Properties props, Double latitude, Double longitude) {
        this.props = props;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setButton(Button button){
        this.getWeatherButton = button;
        button.setOnAction(actionEvent -> {
            try {
                showWeatherDataInApp();
                //displayWildfireNameInApp(wildfire);  <-- TODO:: showPlaceNameInApp()
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public WeatherRoot getWeatherDataFromWeb() throws IOException {

        //Mapper to map json data to object
        ObjectMapper mapper = new ObjectMapper();
        WeatherDao weatherDao = new WeatherDao();
        var jsonAsString = weatherDao.getWeatherDataFromWebApi(props, latitude, longitude);

        try{
            weatherRoot = mapper.readValue(jsonAsString, WeatherRoot.class);
            return weatherRoot;
        } catch(IOException e){
            e.printStackTrace();
            return null;
        }
    }


    public void displayWildfireNameInApp(Wildfire wildfire){
        weatherPlaceNameLabel.setText("Wildfire location: " + wildfire.getName());
    }

    public void emptyWeatherTableFields(){
        weatherLists.clear();
    }

    public void showWeatherDataInApp() throws IOException {
        emptyWeatherTableFields();
        weatherLists.addAll(getWeatherDataFromWeb().getList());
        weatherDateTimeCol.setCellValueFactory(new PropertyValueFactory<>("dt_txt"));
        weatherTempCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(String.valueOf(data
                .getValue()
                .getMain()
                .getTemp())));
        weatherWindSpeedCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(String.valueOf(data
                .getValue()
                .getWind()
                .getSpeed())));
        weatherWindDirectionCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(String.valueOf(data
                .getValue()
                .getWind()
                .getDeg())));
        weatherHumidityCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(String.valueOf(data
                .getValue()
                .getMain()
                .getHumidity())));
        weatherTable.setItems(weatherLists);
    }


}
