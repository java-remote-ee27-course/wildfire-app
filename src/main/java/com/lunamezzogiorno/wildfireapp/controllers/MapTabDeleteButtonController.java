package com.lunamezzogiorno.wildfireapp.controllers;

import com.lunamezzogiorno.wildfireapp.data.dao.RescuerDao;
import com.lunamezzogiorno.wildfireapp.data.dao.WildfireDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Rescuer;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MapTabDeleteButtonController {
    @FXML private Button deleteWildfireButton;
    @FXML private TableView<Wildfire> wildfireTable;
    private SessionFactory session;
    private Wildfire wildfire;
    private WildfireDao wildfires;


    public MapTabDeleteButtonController(SessionFactory session) {
        this.session = session;
    }

    public void setButton(Button button){
        deleteWildfireButton = button;
        deleteWildfireButton.setOnAction(actionEvent -> {
            var ses = session.openSession();
            var tx = ses.beginTransaction();
            removeFireFromEverywhere(ses);

            tx.commit();
            ses.close();
        });
    }

    public Wildfire getSelectedFire(){
        return wildfireTable.getSelectionModel().getSelectedItem();
    }

    public void removeFireFromMapTable(){
            wildfireTable.getItems().remove(getSelectedFire());
    }

    public void removeFireFromDatabaseTable(Session ses){
        wildfire = getSelectedFire();
        wildfires = new WildfireDao(ses);
        wildfires.delete(wildfire);
    }

    public void removeFireFromEverywhere(Session ses){
        try{
            removeFireFromDatabaseTable(ses);
            removeFireFromMapTable();
            clearSelection();

        } catch (NullPointerException e){
            var alert = new Alert(Alert.AlertType.NONE);
            ButtonType type = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
            alert.getDialogPane().getButtonTypes().add(type);
            alert.setContentText("\nTo delete a wildfire, please select a wildfire first!\n\n");
            alert.setTitle("Alert");
            alert.showAndWait();

            System.out.println(
                    e.getCause() + ", " +
                    e.getMessage() + " " +
                    e.getClass() + "\n-----\n" +
                    "No wildfire is selected in the Map tab to delete it.\n" +
                    "Please select a wildfire first before pressing DELETE!");
            e.printStackTrace();
        }
    }

    //to clear selection after delete in order not to be able to "update" deleted row:
    public void clearSelection(){
        wildfireTable.getSelectionModel().clearSelection();
    }
}
