package com.lunamezzogiorno.wildfireapp.weatherdata;

import lombok.Data;

@Data
public class Coord {
    private double lat;
    private double lon;
}
