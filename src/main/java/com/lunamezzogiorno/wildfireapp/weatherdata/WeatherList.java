package com.lunamezzogiorno.wildfireapp.weatherdata;

import java.util.ArrayList;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class WeatherList {
    private int dt;
    private WeatherMain main;
    private ArrayList<Weather> weather;
    private Clouds clouds;
    private Wind wind;
    private int visibility;
    private double pop;
    private Sys sys;
    private String dt_txt;
    private Rain rain;
}
