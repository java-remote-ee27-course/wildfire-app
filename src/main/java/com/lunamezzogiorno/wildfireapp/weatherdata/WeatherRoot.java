package com.lunamezzogiorno.wildfireapp.weatherdata;

import lombok.Data;
import java.util.ArrayList;
@Data
public class WeatherRoot {
    private String cod;
    private int message;
    private int cnt;
    private ArrayList<WeatherList> list;
    private City city;
}
