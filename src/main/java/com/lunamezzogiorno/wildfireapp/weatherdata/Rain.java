package com.lunamezzogiorno.wildfireapp.weatherdata;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Rain {
    @JsonProperty("3h")
    private double _3h;
}
