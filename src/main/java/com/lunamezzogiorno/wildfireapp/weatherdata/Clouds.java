package com.lunamezzogiorno.wildfireapp.weatherdata;

import lombok.Data;

@Data
public class Clouds {
    private int all;
}
