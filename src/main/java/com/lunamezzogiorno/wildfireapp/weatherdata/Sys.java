package com.lunamezzogiorno.wildfireapp.weatherdata;

import lombok.Data;

@Data
public class Sys {
    private String pod;
}
