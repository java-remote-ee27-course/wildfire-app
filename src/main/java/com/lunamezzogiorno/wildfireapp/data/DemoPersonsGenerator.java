package com.lunamezzogiorno.wildfireapp.data;



import com.lunamezzogiorno.wildfireapp.data.dao.AddressDao;
import com.lunamezzogiorno.wildfireapp.data.dao.PersonDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import com.lunamezzogiorno.wildfireapp.data.utils.HibernateUtil;
import org.hibernate.SessionFactory;
import java.util.*;
//before fetching the people from db "Person" table, some persons are needed, so I create them here.
//If you set HibernateUtil HBM2DDL_AUTO to "validate" and pre-populate Person table before running the app,
// this class can be deleted


public class DemoPersonsGenerator {

    private AddressDao addresses;
    private PersonDao people;

    public void createPersons(Properties props){
        SessionFactory session = HibernateUtil.getSessionFactory(props);

        Address address1 = new Address("P. Palermo", "Mondello", "V. Flora");
        Address address2 = new Address("P. Palermo", "Palermo", "V. Trieste");
        Address address3 = new Address("P. Trapani", "Erice", "V. Enea");

        Location location1 = new Location(38.199286, 13.318914);
        Location location2 = new Location(38.110854, 13.365075);
        Location location3 = new Location(38.038375, 12.589134);

        address1.setLocation(location1);
        address2.setLocation(location2);
        address3.setLocation(location3);

        Person person1 = new Person("Maria", "Rosso", address1);
        Person person2 = new Person( "Elena", "Salina", address2);
        Person person3 = new Person("Michele", "Topi", address3);

        List<Address> addressesInDatabase = new ArrayList<>(List.of(address1, address2, address3));
        List<Person> peopleInDatabase = new ArrayList<>(List.of(person1, person2, person3));

        var ses = session.openSession();
        var tx = ses.beginTransaction();
        addresses = new AddressDao(ses);
        people = new PersonDao(ses);

        addresses.saveAll(addressesInDatabase);
        people.saveAll(peopleInDatabase);

        tx.commit();
        ses.close();
    }
}
