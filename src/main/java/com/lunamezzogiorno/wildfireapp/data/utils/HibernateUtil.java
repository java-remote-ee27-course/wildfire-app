package com.lunamezzogiorno.wildfireapp.data.utils;

import com.lunamezzogiorno.wildfireapp.data.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;
    public static SessionFactory getSessionFactory(Properties properties){
        if(sessionFactory == null){
            try {
                var configuration = new Configuration();
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://" +
                        properties.getProperty("DB_IP") + ":" +
                        properties.getProperty("DB_PORT") + "/wildfire?serverTimezone=UTC");
                settings.put(Environment.USER, properties.getProperty("DB_USER"));
                settings.put(Environment.PASS, properties.getProperty("DB_PASS"));
                settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");
                settings.put(Environment.SHOW_SQL, "true");
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.HBM2DDL_AUTO, "create");
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(Person.class);
                configuration.addAnnotatedClass(Location.class);
                configuration.addAnnotatedClass(Radius.class);
                configuration.addAnnotatedClass(Firestation.class);
                configuration.addAnnotatedClass(Wildfire.class);
                configuration.addAnnotatedClass(Address.class);
                configuration.addAnnotatedClass(Phone.class);
                configuration.addAnnotatedClass(Rescuer.class);

                var serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties())
                        .build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch(Exception e){
                System.out.println("[ERROR] Error:" + e);
            }
        }
        return sessionFactory;
    }

    public static void shutdown(){
        sessionFactory.close();
    }

}

