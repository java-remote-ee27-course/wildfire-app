package com.lunamezzogiorno.wildfireapp.data;

import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * NB! These calculations do not consider Earth's curvature and unevenness of the ground (e.g. mountains, valleys)
 * however for the short distances (a district) the curvature is not significant.
 * As the unevenness of the ground makes distances longer, so it is ok for calculating Red Zones.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RedZoneCalculations {
    Wildfire wildfire;
    List<Person> people;

    public RedZoneCalculations(Wildfire wildfire) {
        this.wildfire = wildfire;
    }

    public Location getResidentLocation(Person person){
        return people
                .stream()
                .filter(p -> p.getId().equals(person.getId()))
                .map(p -> p.getAddress().getLocation())
                .findFirst()
                .orElse(null);
    }

    public List<Person> getDataOfResidentsInRedZone(){
        List<Person> peopleInRedZone = new ArrayList<>();
        try{
            for(Person p : people) {
                Address address = p.getAddress();
                Location location = address.getLocation();
                Double distance = countDistanceFromFireCenter(location);
                if(isLocationInRedZone(distance)){
                    peopleInRedZone.add(p);
                }
            }

        } catch (NullPointerException e){
            var stackTrace = e.getStackTrace();
            System.out.println("No address added to some persons: " +
                    e.getMessage() +
                    ", in class: " +
                    e.getClass());
            System.out.println(" in method: " +
                    stackTrace[0].getMethodName() + "in row: " +
                    Arrays.stream(stackTrace).findFirst());
        }

        return peopleInRedZone;
    }

    public boolean isLocationInRedZone(Double distanceFromCenter){
        return distanceFromCenter - getRedZoneRadius() <= 0;
    }

    //rough calculations of distance btwn two points;
    //distance is multiplied with 100_000 to get meters
    public Double countDistanceFromFireCenter(Location residentLocation){

        double distance = Math.pow(residentLocation.getLatitude() - getFireLocation().getLatitude(), 2) +
                Math.pow(residentLocation.getLongitude() - getFireLocation().getLongitude(), 2);
        return Math.sqrt(distance) * 100_000;
    }

    public Double getRedZoneRadius(){
            return getFireRadius() * getCoefficient();
    }

    public Double getCoefficient(){
        if(wildfire.getRedZoneCoefficient() != null){
            return wildfire.getRedZoneCoefficient();
        }
        return 0.0D;
    }

    public Double getFireRadius(){

        var radiusEntity = wildfire.getRadius();
        if(radiusEntity.getRadius() != null)
            return radiusEntity.getRadius();
        return 0.0D;
    }

    public Location getFireLocation(){
        return wildfire.getLocation();
    }

}
