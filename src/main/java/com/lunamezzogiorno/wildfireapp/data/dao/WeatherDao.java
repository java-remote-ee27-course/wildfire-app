package com.lunamezzogiorno.wildfireapp.data.dao;

import org.hibernate.Session;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

/**
 * Found the ideas from:
 * https://www.codejava.net/java-se/networking/how-to-use-java-urlconnection-and-httpurlconnection
 * https://stackoverflow.com/questions/39759492/weather-api-and-httpurlconnection
 */
public class WeatherDao {
    //https://api.openweathermap.org/data/3.0/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API key}
    Session session;

    public String getWeatherDataFromWebApi(Properties props, Double latitude, Double longitude) throws IOException {
        HttpURLConnection httpCon = null;
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            URL url = new URI("https://api.openweathermap.org/data/2.5/forecast?"
                    + "lat=" + latitude
                    + "&lon=" + longitude
                    + "&appid=" + props.getProperty("OPENWEATHERMAP_API_KEY")
                    + "&units=metric").toURL();

            httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setRequestMethod("GET");
            httpCon.setDoInput(true);
            httpCon.connect();

            inputStream = httpCon.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                sb.append(inputLine).append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) bufferedReader.close();
            if (inputStream != null) inputStream.close();
            if (httpCon != null) httpCon.disconnect();
        }
        return sb.toString();
    }
}


