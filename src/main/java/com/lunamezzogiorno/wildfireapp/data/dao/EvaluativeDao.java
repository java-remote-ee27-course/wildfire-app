package com.lunamezzogiorno.wildfireapp.data.dao;

public interface EvaluativeDao<T> {
    Boolean isPresent(Double[] values);
}
