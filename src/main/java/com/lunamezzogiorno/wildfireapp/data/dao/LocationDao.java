package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.query.SelectionQuery;


import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class LocationDao implements UpdatableDao<Location, Double>, SelectableDao<Location, Double>, EvaluativeDao<Double>{
    private Session session;

    @Override
    public void save(Location location) {
        session.persist(location);
    }

    @Override
    public void saveAll(List<Location> locations) {
        locations.forEach(this::save);
    }

    @Override
    public void update(Location location, Double[] parameters) {
        var locationFound = session.find(Location.class, location.getId());
        if(locationFound != null) {
            locationFound.setLatitude(parameters[0]);
            locationFound.setLongitude(parameters[1]);
        }
        session.persist(locationFound);
        session.flush();
    }

    @Override
    public void delete(Location location) {
        // TODO::to be implemented
    }

    @Override
    public Optional<Location> get(Double[] values) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Location> cr = cb.createQuery(Location.class);
        Root<Location> root = cr.from(Location.class);
        var predicate = cb.and(root.get("latitude").in(values[0]),
                              (root.get("longitude").in(values[1])));

        var location = cr.select(root).where(predicate);
        Query<Location> query = session.createQuery(location);
        List<Location> results = query.getResultList();
        return Optional.of(results.get(0));
    }

    @Override
    public List<Location> getAll() {
        SelectionQuery<Location> locations =
                session.createSelectionQuery("From Location", Location.class);
        return locations.getResultList();
    }

    @Override
    public Boolean isPresent(Double[] location) {
        for(Location l : getAll())
            if(l.getLatitude().equals(location[0]) && l.getLongitude().equals(location[1])) {
                return true;
            }
        return false;
    }
}
