package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Person;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.query.SelectionQuery;

import java.util.List;
import java.util.Optional;

/**
 *
 */
@AllArgsConstructor
public class PersonDao implements UpdatableDao<Person, String>, SelectableDao<Person, String> {
    private Session session;

    @Override
    public void save(Person person) {
        session.persist(person);
    }

    @Override
    public void saveAll(List<Person> people) {
        people.forEach(p -> session.persist(p));
    }

    @Override
    public void update(Person person, String[] parameters) {
        // TODO::to be implemented
    }

    @Override
    public void delete(Person person) {
        // to be implemented
    }

    @Override
    public Optional<Person> get(String[] values) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Person> cr = cb.createQuery(Person.class);
        Root<Person> root = cr.from(Person.class);
        var person = cr.select(root).where(root.get("person").in(values[0]));
        Query<Person> query = session.createQuery(person);
        List<Person> results = query.getResultList();
        return Optional.of(results.get(0));
    }

    @Override
    public List<Person> getAll() {
        SelectionQuery<Person> people =
                session.createSelectionQuery("From Person", Person.class);
        return people.getResultList();
    }
}
