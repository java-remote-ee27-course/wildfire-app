package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Radius;
import jakarta.persistence.criteria.*;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;


import java.util.List;
import java.util.Optional;


@AllArgsConstructor
public class RadiusDao implements UpdatableDao<Radius, Double>, SelectableDao<Radius, Double>, EvaluativeDao<Double> {
    private Session session;

    @Override
    public void save(Radius object) {
        session.persist(object);
    }

    @Override
    public void saveAll(List<Radius> radii) {
        radii.forEach(this::save);
    }

    @Override
    public void update(Radius radius, Double[] parameters) {
        var radiusFound = session.find(Radius.class, radius.getId());
        if(radiusFound != null) {
            radiusFound.setRadius(parameters[0]);
        }
        session.persist(radiusFound);
        session.flush();
    }

    @Override
    public void delete(Radius radius) {
        //to be implemented
    }

    @Override
    public Optional<Radius> get(Double [] r) {
        // gets optional Radius object from provided Double array.
        // As Radius object has only radius field (except id)
        // then the array contains only 1 elem: radius
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Radius> cr = cb.createQuery(Radius.class);
        Root<Radius> root = cr.from(Radius.class);
        var radius = cr.select(root).where(root.get("radius").in(r[0]));
        Query<Radius> query = session.createQuery(radius);
        List<Radius> results = query.getResultList();
        return Optional.of(results.get(0));
    }

    @Override
    public List<Radius> getAll() {
        var radii = session.createSelectionQuery("From Radius", Radius.class);
        return radii.getResultList();
    }

    @Override
    public Boolean isPresent(Double[] values) {
        for(Radius r : getAll())
            if(r.getRadius().equals(values[0]))
                return true;
        return false;
    }
}
