package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.query.SelectionQuery;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class WildfireDao implements UpdatableDao<Wildfire, Object>, SelectableDao<Wildfire, Object> {
    private Session session;
    @Override
    public void save(Wildfire wildfire) {
        session.persist(wildfire);
    }

    @Override
    public void saveAll(List<Wildfire> wildfires) {
        wildfires.forEach(this::save);
    }

    @Override
    public void update(Wildfire wildfire, Object[] parameters) {
        var wildfireFound = session.find(Wildfire.class, wildfire.getId());
        if(wildfireFound != null) {
            wildfireFound.setName((String) parameters[0]);
            wildfireFound.setRedZoneCoefficient((Double) parameters[1]);
        }
        session.persist(wildfireFound);
        session.flush();

    }

    @Override
    public void delete(Wildfire wildfire) {
        var deletableFire = session.find(Wildfire.class, wildfire.getId());
        if(deletableFire != null) {
            deletableFire.removeAllAssociations();
            deletableFire.getRescuers().clear();
            session.remove(deletableFire);
            session.flush();
        }
    }

    @Override
    public Optional<Wildfire> get(Object[] values) {
        //TODO:: add later some other values to find wf, e.g. name, loc
        var wildfire = session.find(Wildfire.class, values[0]);
        if(wildfire != null){
            return Optional.of(wildfire);
        }
        return Optional.empty();
    }

    @Override
    public List<Wildfire> getAll() {
        // Left Join used to keep away the LazyInitializationException
        // See https://thorben-janssen.com/lazyinitializationexception/

        SelectionQuery<Wildfire> wildfire =
                session.createSelectionQuery("From Wildfire", Wildfire.class);
        return wildfire.getResultList();
    }
}
