package com.lunamezzogiorno.wildfireapp.data.dao;

import java.util.List;
import java.util.Optional;


public interface SelectableDao<T, V> {

        Optional<T> get(V[] values);
        List<T> getAll();

}
