package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Rescuer;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.SelectionQuery;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class RescuerDao implements UpdatableDao<Rescuer, Double>, SelectableDao<Rescuer, Object>{
    private Session session;

    @Override
    public void save(Rescuer rescuer) {
        session.persist(rescuer);
    }

    @Override
    public void saveAll(List<Rescuer> rescuers) {
        rescuers.forEach(r -> session.persist(r));
    }

    @Override
    public void update(Rescuer rescuer, Double[] parameters) {

    }

    @Override
    public void delete(Rescuer rescuer) {

    }

    @Override
    public Optional<Rescuer> get(Object[] values) {
        return Optional.empty();
    }

    @Override
    public List<Rescuer> getAll() {
        SelectionQuery <Rescuer> roles = session.createSelectionQuery("FROM Rescuer r" , Rescuer.class); //LEFT JOIN FETCH r.firestations", Rescuer.class);
        return roles.getResultList();
    }
}
