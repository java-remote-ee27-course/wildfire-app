package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import com.lunamezzogiorno.wildfireapp.data.entities.Firestation;
import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Radius;
import eu.hansolo.tilesfx.tools.Fire;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.query.SelectionQuery;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class FirestationDao implements SelectableDao<Firestation, Object>{
    private Session session;

    @Override
    public Optional<Firestation> get(Object[] values) {
        //Not yet functioning, to be added later:
        String name = (String)values[0];
        Location location = (Location) values[1];
        //var stationById = session.find(values[0]);
        return Optional.empty();
    }

    public Firestation getFirestationByName(String firestationName){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Firestation> cr = cb.createQuery(Firestation.class);
        Root<Firestation> root = cr.from(Firestation.class);
        var firestation = cr.select(root).where(root.get("name").in(firestationName));
        Query<Firestation> query = session.createQuery(firestation);
        return query.getResultList().get(0);
    }

    @Override
    public List<Firestation> getAll() {
        // Left Join used to keep away the LazyInitializationException
        // See https://thorben-janssen.com/lazyinitializationexception/
        SelectionQuery<Firestation> firestations =
                session.createSelectionQuery("FROM Firestation f LEFT JOIN FETCH f.phones", Firestation.class);
        return firestations.getResultList();
    }


    public void saveAll(List<Firestation> firestations){
        firestations.forEach(f -> session.persist(f));
    }
    public void save(Firestation firestation){
        session.persist(firestation);
    }

    public void update(Firestation firestation, Object[] parameters){
        var firestationFound = session.find(Firestation.class, firestation.getId());
        if(firestationFound != null) {
            firestationFound.setName(parameters[0].toString());
        }
        session.persist(firestationFound);
        session.flush();
    }
}
