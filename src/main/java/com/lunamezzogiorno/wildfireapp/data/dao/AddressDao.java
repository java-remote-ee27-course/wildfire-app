package com.lunamezzogiorno.wildfireapp.data.dao;

import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class AddressDao implements UpdatableDao<Address, String>, SelectableDao<Address, String> {
    private Session session;

    @Override
    public void save(Address address) {
        session.persist(address);
    }

    @Override
    public void saveAll(List<Address> addresses) {
        addresses.forEach(this::save);
    }


    //TODO::The update() method is not yet used anywhere(?) (except in unit tests):
    @Override
    public void update(Address address, String[] parameters) {
        var addressFound = session.find(Address.class, address.getId());
        if(addressFound != null){
            addressFound.setDistrict(parameters[0]);
        }
    }

    @Override
    public void delete(Address address) {
        //to be implemented

    }

    //TODO::The get() method is not yet used anywhere (except in unit tests):
    @Override
    public Optional<Address> get(String[] addresses) {
        //TODO:: the address contains more fields than one, edit the parameter array later
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Address> cr = cb.createQuery(Address.class);
        Root<Address> root = cr.from(Address.class);
        var address = cr.select(root).where(root.get("district").in(addresses[0]));
        Query<Address> query = session.createQuery(address);
        List<Address> results = query.getResultList();
        return Optional.of(results.get(0));
    }

    public List<Address> getAddressByCity(String cityName){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Address> cr = cb.createQuery(Address.class);
        Root<Address> root = cr.from(Address.class);
        var address = cr.select(root).where(root.get("city").in(cityName));
        Query<Address> query = session.createQuery(address);
        return query.getResultList();
    }

    @Override
    public List<Address> getAll() {
        var addresses = session.createSelectionQuery("From Address", Address.class);
        return addresses.getResultList();
    }
}
