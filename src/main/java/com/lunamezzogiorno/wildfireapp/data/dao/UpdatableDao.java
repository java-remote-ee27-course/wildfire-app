package com.lunamezzogiorno.wildfireapp.data.dao;

import java.util.List;

public interface UpdatableDao<T,V> {
        void save(T t);
        void saveAll(List<T> t);
        void update(T t, V[] parameters);
        void delete(T t);

}
