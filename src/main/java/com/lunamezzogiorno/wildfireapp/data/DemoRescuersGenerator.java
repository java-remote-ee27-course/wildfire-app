package com.lunamezzogiorno.wildfireapp.data;

import com.lunamezzogiorno.wildfireapp.data.dao.*;
import com.lunamezzogiorno.wildfireapp.data.entities.*;
import com.lunamezzogiorno.wildfireapp.data.utils.HibernateUtil;
import org.hibernate.SessionFactory;

import java.util.*;

public class DemoRescuersGenerator {
    private Rescuer rescuerCammilleri;
    private Rescuer rescuerZerullo;
    private Rescuer rescuerTaranto;
    private Rescuer rescuerMonti;
    private Location vvFTrapaniFirestationLocation;
    private Location vdFCorleoneFirestationLocation;
    private Address vvFTrapaniFirestationAddress;
    private Address vdFCorleoneFirestationAddress;
    private Firestation vvFTrapaniFirestation;
    private Firestation vdFCorleoneFirestation;
    private Person gianniCammilleri;
    private Person paoloTaranto;
    private Person marcoZerullo;
    private Person andreaMonti;
    private Address gianniCammilleriHomeAddress;
    private Address paoloTarantoHomeAddress;
    private Address marcoZerulloHomeAddress;
    private Address andreaMontiHomeAddress;
    private Wildfire corleoneFire;
    private Wildfire trapaniFire;
    private Radius corleoneFireRadius;
    private Radius trapaniFireRadius;



    public void createRescuers(Properties props){
        SessionFactory session = HibernateUtil.getSessionFactory(props);

        createRescuersAndConnections();

        var ses = session.openSession();
        var tx = ses.beginTransaction();

        RescuerDao rescuerDao = new RescuerDao(ses);
        PersonDao personDao = new PersonDao(ses);
        AddressDao addressDao = new AddressDao(ses);
        FirestationDao firestationDao = new FirestationDao(ses);
        RadiusDao radiusDao = new RadiusDao(ses);
        WildfireDao wildfireDao = new WildfireDao(ses);

        radiusDao.save(corleoneFireRadius);
        wildfireDao.save(corleoneFire);

        List<Person> persons = new ArrayList<>(List.of(gianniCammilleri, paoloTaranto, marcoZerullo, andreaMonti));
        personDao.saveAll(persons);

        List<Address> addresses = new ArrayList<>(List.of(
                gianniCammilleriHomeAddress,
                paoloTarantoHomeAddress,
                marcoZerulloHomeAddress,
                andreaMontiHomeAddress));
        addressDao.saveAll(addresses);

        List <Firestation> firestations = new ArrayList<>(List.of(vvFTrapaniFirestation, vdFCorleoneFirestation));
        firestationDao.saveAll(firestations);

        List<Rescuer> rescuers = new ArrayList<>(List.of(rescuerCammilleri,
                                                         rescuerZerullo,
                                                         rescuerTaranto,
                                                         rescuerMonti));
        rescuerDao.saveAll(rescuers);

        tx.commit();
        ses.close();
    }

    public void createRescuersAndConnections(){
        corleoneFireRadius = new Radius(20.0D);
        corleoneFire = new Wildfire("Corleone fire",
                       new Location(37.816133, 13.308618),
                      30D,
                       corleoneFireRadius);

        rescuerCammilleri = new Rescuer();
        rescuerTaranto = new Rescuer();
        rescuerZerullo = new Rescuer();
        rescuerMonti = new Rescuer();
        rescuerMonti.setWildfire(corleoneFire);

        Set <Rescuer> rescuersOfCorleoneFire = new HashSet<>();
        rescuersOfCorleoneFire.add(rescuerMonti);
        corleoneFire.setRescuers(rescuersOfCorleoneFire);

        vvFTrapaniFirestationLocation = new Location(38.01375, 12.51579);
        vdFCorleoneFirestationLocation = new Location(37.821844, 13.289790);
        vvFTrapaniFirestationAddress = new Address("P. Trapani", "Trapani", "V. A. Staiti 101");
        vdFCorleoneFirestationAddress = new Address("P. Palermo", "Corleone", "SP4, Ficuzza");
        vvFTrapaniFirestation = new Firestation("VV. F di Trapani");
        vdFCorleoneFirestation = new Firestation("V. del F Corleone");

        gianniCammilleriHomeAddress = new Address("P. Trapani", "Trapani", "V. Illio");
        paoloTarantoHomeAddress = new Address("P. Trapani", "Trapani", "V. Pantelleria");
        marcoZerulloHomeAddress = new Address("P. Trapani", "Trapani", "V. Archi");
        andreaMontiHomeAddress = new Address("P. Palermo", "Corleone", "V. Roma");

        gianniCammilleriHomeAddress.setLocation(new Location(38.014907, 12.520750));
        paoloTarantoHomeAddress.setLocation(new Location(38.022789, 12.535698));
        marcoZerulloHomeAddress.setLocation(new Location(38.022450, 12.527212));
        andreaMontiHomeAddress.setLocation(new Location(37.813068, 13.306110));

        gianniCammilleri = new Person("Gianni", "Cammilleri", gianniCammilleriHomeAddress);
        paoloTaranto = new Person("Paolo", "Taranto", paoloTarantoHomeAddress);
        marcoZerullo = new Person("Marco", "Zerullo", marcoZerulloHomeAddress);
        andreaMonti = new Person("Andrea", "Monti", andreaMontiHomeAddress);

        //firefighters: connect rescuers to persons:
        gianniCammilleri.setRescuer(rescuerCammilleri);
        paoloTaranto.setRescuer(rescuerTaranto);
        marcoZerullo.setRescuer(rescuerZerullo);
        andreaMonti.setRescuer(rescuerMonti);
        rescuerCammilleri.setPerson(gianniCammilleri);
        rescuerTaranto.setPerson(paoloTaranto);
        rescuerZerullo.setPerson(marcoZerullo);
        rescuerMonti.setPerson(andreaMonti);

        //connect firestations to rescuers:
        rescuerCammilleri.setFirestation(vvFTrapaniFirestation);
        rescuerTaranto.setFirestation(vvFTrapaniFirestation);
        rescuerZerullo.setFirestation(vvFTrapaniFirestation);
        rescuerMonti.setFirestation(vdFCorleoneFirestation);

        vvFTrapaniFirestation.setLocation(vvFTrapaniFirestationLocation);
        vdFCorleoneFirestation.setLocation(vdFCorleoneFirestationLocation);
        vvFTrapaniFirestation.setAddress(vvFTrapaniFirestationAddress);
        vdFCorleoneFirestation.setAddress(vdFCorleoneFirestationAddress);
    }
}
