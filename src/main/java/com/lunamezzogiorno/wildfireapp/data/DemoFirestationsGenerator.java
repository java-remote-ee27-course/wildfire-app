package com.lunamezzogiorno.wildfireapp.data;


import com.lunamezzogiorno.wildfireapp.data.dao.FirestationDao;
import com.lunamezzogiorno.wildfireapp.data.dao.LocationDao;
import com.lunamezzogiorno.wildfireapp.data.entities.*;
import com.lunamezzogiorno.wildfireapp.data.utils.HibernateUtil;
import org.hibernate.SessionFactory;

import java.util.*;
//before fetching the Firestations from db, some firestations are needed, so I create them here.
//If you set HibernateUtil HBM2DDL_AUTO to "validate" and pre-populate Firestation table before running the app,
//this class can be deleted

public class DemoFirestationsGenerator {

    private LocationDao locations;
    private FirestationDao firestations;

    public void createFirestations(Properties properties){
        SessionFactory session = HibernateUtil.getSessionFactory(properties);
        Location loc1 = new Location(38.16335, 13.32955);
        Location loc2 = new Location(38.11910, 13.35746);
        Location loc3 = new Location(38.01121, 12.57777);
        Address address1 = new Address("P. Palermo", "Palermo", "V. d. Quartieri 102");
        Address address2 = new Address("P. Palermo", "Palermo", "V. A. Scarlatti 16");
        Address address3 = new Address("P. Trapani", "Trapani", "C. da Birgi 91100");
        Firestation fs1 = new Firestation("Palermo Nord", loc1, address1);
        Firestation fs2 = new Firestation( "Palermo C. Prov.", loc2, address2);
        Firestation fs3 = new Firestation("C. Prov. Trapani", loc3, address3);
        Phone phone1 = new Phone(1122);
        Phone phone2 = new Phone(1123);
        Phone phone3 = new Phone(1124);
        Phone phone4 = new Phone(1125);
        fs1.setPhones(new HashSet<>(Set.of(phone1, phone2)));
        fs2.setPhones(new HashSet<>(Set.of(phone2, phone3)));
        fs3.setPhones(new HashSet<>(Set.of(phone4)));
        phone1.setFirestations(new HashSet<>(Set.of(fs1)));
        phone2.setFirestations(new HashSet<>(Set.of(fs1, fs2)));
        phone3.setFirestations(new HashSet<>(Set.of(fs2)));
        phone4.setFirestations(new HashSet<>(Set.of(fs3)));

        List<Location> fsLocations = new ArrayList<>(List.of(loc1, loc2, loc3));
        List<Firestation> localFirestations = new ArrayList<>(List.of(fs1, fs2, fs3));


        var ses = session.openSession();
        var tx = ses.beginTransaction();
        locations = new LocationDao(ses);
        firestations = new FirestationDao(ses);

        locations.saveAll(fsLocations);
        firestations.saveAll(localFirestations);

        tx.commit();
        ses.close();
    }
}
