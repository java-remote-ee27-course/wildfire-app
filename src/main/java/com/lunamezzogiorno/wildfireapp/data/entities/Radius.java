package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Entity
@NoArgsConstructor
public class Radius {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double radius;

    public Radius(Double radius) {
        this.radius = radius;
    }
    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if ((o == null) || (o.getClass() != this.getClass())) return false;
        Radius r = (Radius) o;
        return this.radius.equals(r.radius);
    }

    @Override
    public int hashCode() {
        return radius != null ? radius.hashCode() : 0;
    }
}

