package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

/**
 * Inheritance: Location is parent, RedZone child
 * See more about @Inheritance, @DiscriminatorColumn
 * https://www.baeldung.com/hibernate-inheritance
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Location {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double latitude;
    private Double longitude;
    //private Double redZoneCoefficient;

    @OneToOne(mappedBy = "location")
    private Address address;

    @OneToOne(mappedBy = "location")
    private Firestation firestation;

    @OneToOne(mappedBy = "location")
    private Wildfire wildfire;


    public Location(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if ((o == null) || (o.getClass() != this.getClass())) return false;
        Location l = (Location) o;
        return this.latitude.equals(l.latitude) &&
                this.longitude.equals(l.longitude);
    }

    @Override
    public int hashCode() {
        int result = latitude != null ? latitude.hashCode() : 0;
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
