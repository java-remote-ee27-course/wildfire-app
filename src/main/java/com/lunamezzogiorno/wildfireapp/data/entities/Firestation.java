package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

//Fetch type eager:
//https://www.baeldung.com/hibernate-initialize-proxy-exception
//fetch type eager is needed to fetch firestations locations data to firestations fx tableview

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Firestation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "firestation_location",
               joinColumns = {@JoinColumn(
                      name = "firestation_id",
                      referencedColumnName = "id")},
               inverseJoinColumns = { @JoinColumn(
                      name = "location_id",
                      referencedColumnName = "id")})
    private Location location;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "firestation_address",
            joinColumns = {@JoinColumn(
                    name = "firestation_id",
                    referencedColumnName = "id")},
            inverseJoinColumns = { @JoinColumn(
                    name = "address_id",
                    referencedColumnName = "id")})
    private Address address;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "firestation_phone",
               joinColumns = @JoinColumn(name = "firestation_id"),
               inverseJoinColumns = @JoinColumn(name = "phone_id"))
    private Set<Phone> phones = new HashSet<>();

    @OneToMany(mappedBy = "firestation", cascade = CascadeType.ALL)
    private Set<Rescuer> rescuers = new HashSet<>();

    public Firestation(String name) {
        this.name = name;
    }

    public Firestation(String name, Location location) {
        this.name = name;
        this.location = location;
    }
    public Firestation(String name, Location location, Address address) {
        this.name = name;
        this.location = location;
        this.address = address;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Firestation that = (Firestation) o;

        return Objects.equals(location, that.location);
    }

    @Override
    public int hashCode() {
        return location != null ? location.hashCode() : 0;
    }
}
