package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Wildfire {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Double redZoneCoefficient;
    @OneToOne(cascade = CascadeType.ALL) //, fetch = FetchType.LAZY)
    @JoinTable(name = "wildfire_location",
        joinColumns = {@JoinColumn(name = "wildfire_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "location_id", referencedColumnName = "id")})
    private Location location;
    @ManyToOne //(fetch = FetchType.LAZY) TODO:: not a good practice use fetch type EAGER, try to solve it
    @JoinColumn(name = "radius_id", foreignKey = @ForeignKey(name = "RADIUS2_ID_FK"))
    private Radius radius;

    @OneToMany(mappedBy = "wildfire",
            cascade = CascadeType.ALL, fetch = FetchType.EAGER) //TODO:: not a good practice use fetch type EAGER, try to solve it
    private Set<Rescuer> rescuers = new HashSet<>();

    public Wildfire(String name, Location location, Double redZoneCoefficient) {
        this.name = name;
        this.location = location;
        this.redZoneCoefficient = redZoneCoefficient;
    }

    public Wildfire(String name, Location location, Double redZoneCoefficient, Radius radius) {
        this.name = name;
        this.redZoneCoefficient = redZoneCoefficient;
        this.location = location;
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wildfire wildfire = (Wildfire) o;

        return Objects.equals(name, wildfire.name);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @PreRemove
    public void removeAllAssociations(){
        for(Rescuer r : rescuers){
            r.setWildfire(null);
        }
        this.setLocation(null);
    }
}
