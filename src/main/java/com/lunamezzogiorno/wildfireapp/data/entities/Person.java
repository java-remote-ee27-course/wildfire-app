package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

/**
 * Do not add Person-Address FetchType.LAZY, for then no address data will be fetched to "People in Area" tab
 * Change it later from Person DAO to fetch addresses. See the FirestationDao
 * See fetch type eager:
 * https://www.baeldung.com/hibernate-initialize-proxy-exception
 * fetch type eager is needed to fetch persons address data to "People in Area" fx tableview
 * TODO:: add Phone to person
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String lastname;
    @ManyToOne
    @JoinColumn(name = "address_id", foreignKey = @ForeignKey(name = "ADDRESS_ID_FK"))
    private Address address;
    //private Phone phone;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "person_rescuer",
            joinColumns = {
                @JoinColumn(
                    name = "person_id",
                    referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(
                    name = "rescuer_id",
                    referencedColumnName = "id")})
    private Rescuer rescuer;


    public Person(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public Person(String name, String lastname, Address address) {
        this.name = name;
        this.lastname = lastname;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", roles=" + rescuer +
                '}';
    }
}
