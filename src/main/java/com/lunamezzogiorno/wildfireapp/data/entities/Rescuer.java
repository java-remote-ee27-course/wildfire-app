package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
//@ToString
public class Rescuer {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "rescuer")
    private Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "firestation_id")
    private Firestation firestation;

    @ManyToOne //(fetch = FetchType.LAZY) TODO:: fix it later, better is leave it to be lazy initialization exception
    @JoinColumn(name = "wildfire_id")
    private Wildfire wildfire;

    public Rescuer(Person person) {
        this.person = person;
    }

    @PreRemove
    public void removeAllAssociations(){
        wildfire.getRescuers().remove(this);
    }

    @Override
    public String toString() {
        return "Rescuer{" +
                "person=" + person.getName() +
                ", " + person.getLastname() +
                '}';
    }
}
