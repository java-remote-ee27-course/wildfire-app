package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@ToString(exclude = "firestations")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int number;
    @ManyToMany(mappedBy = "phones", fetch = FetchType.LAZY)
    private Set<Firestation> firestations = new HashSet<>();

    public Phone(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if ((o == null) || (o.getClass() != this.getClass())) return false;
        Phone phone = (Phone) o;
        return this.number == phone.number;
    }

    @Override
    public int hashCode() {
        return number;
    }
}
