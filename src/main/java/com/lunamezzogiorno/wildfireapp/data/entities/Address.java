package com.lunamezzogiorno.wildfireapp.data.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String district;
    private String city;
    private String street;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "address_location",
               joinColumns = {@JoinColumn(name = "address_id", referencedColumnName = "id")},
               inverseJoinColumns = {@JoinColumn(name = "location_id", referencedColumnName = "id")})
    private Location location;

    @OneToOne(mappedBy = "address")
    private Firestation firestation;

    public Address(String district) {
        this.district = district;
    }

    public Address(String district, String city, String street) {
        this.district = district;
        this.city = city;
        this.street = street;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if ((o == null) || (o.getClass() != this.getClass())) return false;
        Address a = (Address) o;
        return this.district.equals(a.district) &&
               this.city.equals(a.city) &&
               this.street.equals(a.street);
    }

    @Override
    public int hashCode() {
        int result = district != null ? district.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        return result;
    }
}