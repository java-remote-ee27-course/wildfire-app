package com.lunamezzogiorno.wildfireapp.data;

import com.lunamezzogiorno.wildfireapp.data.dao.LocationDao;
import com.lunamezzogiorno.wildfireapp.data.dao.RadiusDao;
import com.lunamezzogiorno.wildfireapp.data.dao.WildfireDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Location;
import com.lunamezzogiorno.wildfireapp.data.entities.Radius;
import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;
import com.lunamezzogiorno.wildfireapp.data.utils.HibernateUtil;
import lombok.Getter;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DemoWildfireGenerator {

    private LocationDao locations;
    private WildfireDao wildfireActions;
    private RadiusDao radii2;


    public void createWildfires(Properties props){
        SessionFactory session = HibernateUtil.getSessionFactory(props);


        Radius radius1 = new Radius(300.0);
        Radius radius = new Radius(1000.0);

        Location loc1 = new Location(38.11529, 13.35905);
        Location loc2 = new Location(38.02371, 12.54313);


        Wildfire wf1 = new Wildfire("Palermo fire", loc1, 5D, radius1);
        Wildfire wf2 = new Wildfire( "Trapani fire", loc2, 5D, radius);

        List<Location> wfLocations = new ArrayList<>(List.of(loc1, loc2));
        List<Wildfire> wildfires = new ArrayList<>(List.of(wf1, wf2));


        List<Radius> radiuses2 = new ArrayList<>(List.of(radius1, radius));


        var ses = session.openSession();
        var tx = ses.beginTransaction();

        locations = new LocationDao(ses);
        wildfireActions = new WildfireDao(ses);

        radii2 = new RadiusDao(ses);
        radii2.saveAll(radiuses2);

        locations.saveAll(wfLocations);
        wildfireActions.saveAll(wildfires);

        tx.commit();
        ses.close();
    }
}
