package com.lunamezzogiorno.wildfireapp;

import com.lunamezzogiorno.wildfireapp.controllers.AppController;
import com.lunamezzogiorno.wildfireapp.data.*;
import com.lunamezzogiorno.wildfireapp.data.utils.HibernateUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.SessionFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * WildfireApp class holds the main method of Wildfire app,
 * which launches the app by starting it.
 */
public class WildfireApp extends Application {

    private SessionFactory hibernateSession;

    //generate some demo-firestations to database:
    public static void addSomeInitialFirestationData(Properties props){
        var firestationsGenerator = new DemoFirestationsGenerator();
        firestationsGenerator.createFirestations(props);
    }

    //generate some demo-persons to database:
    public static void addSomeInitialPersonsData(Properties props){
        var personsGenerator = new DemoPersonsGenerator();
        personsGenerator.createPersons(props);
    }

    //generate some demo-wildfires to DB:
    public static void addSomeInitialWildfires(Properties props){
        var wildfireGenerator = new DemoWildfireGenerator();
        wildfireGenerator.createWildfires(props);
    }

    public static void createInitialRescuers(Properties props){
        var rolesGenerator = new DemoRescuersGenerator();
        rolesGenerator.createRescuers(props);
    }

    @Override
    public void start(Stage stage) throws IOException {
        var props = new Properties();
        props.load(new FileInputStream("target/classes/app.properties"));
        hibernateSession = HibernateUtil.getSessionFactory(props);
        addSomeInitialFirestationData(props);
        addSomeInitialPersonsData(props);
        addSomeInitialWildfires(props);
        createInitialRescuers(props);

        var resource = WildfireApp.class.getResource("wildfireapp-view.fxml");
        var fxmlLoader = new FXMLLoader(resource);
        var appController = new AppController(hibernateSession);
        fxmlLoader.setController(appController);
        var scene = new Scene(fxmlLoader.load(), 650, 800);

        stage.setTitle("Wildfire app");
        stage.setScene(scene);
        stage.show();
    }

    public void stop() throws Exception {
        HibernateUtil.shutdown();
        super.stop();
    }

    public static void main(String[] args) throws IOException {
        launch();
    }
}