package com.lunamezzogiorno.wildfireapp;

import com.lunamezzogiorno.wildfireapp.data.entities.Wildfire;

/**
 * DataSingleton of eager type is used to create a wildfire instance
 * to carry data between scenes (see classes: MapTabUpdateButtonController,
 * SelectAFireOfWildfiresController and UpdatewildfireViewController
 * and classes: RescuersInAreaTabController, ButtonAddRescuersInAreaController and ...
 *
 * Singleton created by the model: https://www.youtube.com/watch?v=MsgiJdf5njc&ab_channel=Randomcode
 */

public class DataSingleton {
    private static final DataSingleton instance = new DataSingleton();
    private Wildfire wildfire;

    private DataSingleton(){}

    public static DataSingleton getInstance(){
        return instance;
    }

    public Wildfire getWildfire(){
        return wildfire;
    }

    public void setWildfire(Wildfire wildfire) {
        this.wildfire = wildfire;
    }
}
