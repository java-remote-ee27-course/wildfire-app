module com.lunamezzogiorno.wildfireapp {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires java.naming;

    requires javafx.base;
    requires javafx.graphics;

    requires org.controlsfx.controls;
    requires eu.hansolo.tilesfx;

    requires org.hibernate.orm.core;
    requires jakarta.persistence;
    requires static lombok;
    requires org.json;
    requires com.fasterxml.jackson.databind;


    opens com.lunamezzogiorno.wildfireapp to javafx.fxml, org.hibernate.orm.core;
    exports com.lunamezzogiorno.wildfireapp;
    exports com.lunamezzogiorno.wildfireapp.controllers;
    opens com.lunamezzogiorno.wildfireapp.controllers to javafx.fxml, org.hibernate.orm.core;
    exports com.lunamezzogiorno.wildfireapp.data;
    opens com.lunamezzogiorno.wildfireapp.data to javafx.fxml, org.hibernate.orm.core;
    exports com.lunamezzogiorno.wildfireapp.data.dao;
    opens com.lunamezzogiorno.wildfireapp.data.dao to javafx.fxml, org.hibernate.orm.core;
    exports com.lunamezzogiorno.wildfireapp.data.entities;
    opens com.lunamezzogiorno.wildfireapp.data.entities to javafx.fxml, org.hibernate.orm.core;
    exports com.lunamezzogiorno.wildfireapp.data.utils;
    opens com.lunamezzogiorno.wildfireapp.data.utils to javafx.fxml, org.hibernate.orm.core;

    exports com.lunamezzogiorno.wildfireapp.weatherdata to com.fasterxml.jackson.databind;
    opens com.lunamezzogiorno.wildfireapp.weatherdata to javafx.base, com.fasterxml.jackson.databind;

}