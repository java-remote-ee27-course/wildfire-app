package data.dao;

import com.lunamezzogiorno.wildfireapp.data.dao.AddressDao;
import com.lunamezzogiorno.wildfireapp.data.entities.Address;
import com.lunamezzogiorno.wildfireapp.data.utils.HibernateUtil;
import org.assertj.core.api.Assertions;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddressDaoTest {
    AddressDao addressDao;
    HibernateUtil hibernateUtil;
    SessionFactory sessionFactory;
    Session session;
    Properties properties;
    Transaction tx;

    @BeforeAll
    void beforeAll() throws IOException {
        properties = new Properties();
        properties.load(new FileInputStream("target/classes/app.properties"));
        hibernateUtil = new HibernateUtil();
        sessionFactory = hibernateUtil.getSessionFactory(properties);
    }

    @BeforeEach
    void beforeEach() throws IOException {

        session = sessionFactory.openSession();
        System.out.println("beforeEach - Instantiating new addressDao");
        addressDao = new AddressDao(session);
        System.out.println("AddressDao is ready");

    }

    @AfterEach
    void afterEach(){
        session.close();
    }

    @AfterAll
    void afterAll(){
        sessionFactory.close();
    }


    //test saving new Address("Rae", "Assaku", "Jarve tee");
    //by getting later from DB the address addressDao.get(new String[]{"Rae"}).get();
    //(HibernateUtil.java should be set to "create" tables: HBM2DDL_AUTO, "create")

    @Test
    void testSavingAnAddress(){
        //given
        tx =  session.beginTransaction();
        Address address = new Address("Rae", "Assaku", "Jarve tee");

        //when
        addressDao.save(address);
        tx.commit();

        //then
        Assertions.assertThat(addressDao.get(new String[]{"Rae"}).get())
                .isEqualTo(address);
    }

    //(HibernateUtil.java should be set to "create" tables: HBM2DDL_AUTO, "create")
    @Test
    void testUpdateAddressExistingInDatabase(){
        //given
        tx =  session.beginTransaction();
        Address address = new Address("Rae", "Assaku", "Jarve tee");
        addressDao.save(address);
        tx.commit();

        //when
        tx = session.beginTransaction();
        addressDao.update(address, new String[]{"Harjumaa"});
        tx.commit();

        //then
        Assertions.assertThat(addressDao.get(new String[]{"Harjumaa"}).get())
                .isEqualTo(address);
    }

    @Test
    void testGetAddressExistingInDatabase(){
        //given
        tx =  session.beginTransaction();
        Address address = new Address("Rae", "Assaku", "Jarve tee");
        addressDao.save(address);
        tx.commit();
        String [] addresses = {"Rae"};

        //when
        tx =  session.beginTransaction();
        Optional<Address> result = addressDao.get(addresses);
        tx.commit();

        //then
        Assertions.assertThat(result.get())
                .isEqualTo(address)
                .isNotEqualTo("Rae")
                .isNotEqualTo("Assaku")
                .hasFieldOrProperty("district")
                .hasFieldOrProperty("city")
                .hasFieldOrProperty("street");

    }

    @Test
    void testGetAddressNonExistingInDatabase(){
        //given
        String [] addresses = {"Rae"};
        tx =  session.beginTransaction();
        Address address = new Address("Rae", "Assaku", "Jarve tee");
        addressDao.save(address);
        tx.commit();
        Address address2 = new Address("Harjumaa");
        //when

        tx =  session.beginTransaction();
        Optional<Address> result = addressDao.get(addresses);
        tx.commit();

        //then
        Assertions.assertThat(result.get())
                .isNotEqualTo(address2);
    }

    @Test
    void testGetAnAddressFromDatabaseOfMoreRecords(){
        //given

        tx =  session.beginTransaction();
        Address address1 = new Address("Harjumaa", "Assaku", "Jarve tee");
        addressDao.save(address1);
        Address address2 = new Address("Rae", "Assaku", "Jarve tee");
        addressDao.save(address2);
        tx.commit();
        String [] addresses = {"Harjumaa", "Rae"};

        //when

        tx =  session.beginTransaction();
        Optional<Address> result = addressDao.get(addresses);
        tx.commit();

        //then
        Assertions.assertThat(result.get())
                .isNotNull();
    }

    @Test
    void testSaveAllAddresses(){
        //given
        tx =  session.beginTransaction();
        Address address1 = new Address("Harjumaa", "Assaku", "Jarve tee");
        Address address2 = new Address("Rae", "Assaku", "Jarve tee");
        List<Address> addresses = new ArrayList<>(List.of(address1, address2));

        //when
        addressDao.saveAll(addresses);
        String [] someAddresses1 = {"Harjumaa"};
        String [] someAddresses2 = {"Rae"};
        Optional<Address> result1 = addressDao.get(someAddresses1);
        Optional<Address> result2 = addressDao.get(someAddresses2);
        tx.commit();

        //then
        Assertions.assertThat(result1.get())
                .isEqualTo(address1);
        Assertions.assertThat(result2.get())
                .isEqualTo(address2);

    }
    @Test
    void testGetAllAddressesFromDatabase(){
        //given
        tx =  session.beginTransaction();
        Address address1 = new Address("Harjumaa", "Assaku", "Jarve tee");
        Address address2 = new Address("Rae", "Assaku", "Jarve tee");
        List<Address> addresses = new ArrayList<>(List.of(address1, address2));

        //when
        addressDao.saveAll(addresses);
        List<Address> result = addressDao.getAll();
        tx.commit();

        //then
        Assertions.assertThat(result)
                .isEqualTo(addresses);
    }

}
